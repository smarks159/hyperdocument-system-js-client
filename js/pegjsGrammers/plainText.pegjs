text
  = element*

element
  = [ \n\r\t]* e:element2 [ \n\r\t]* {return e}

element2
  = 
  urllink / hyperdoclink / word 

urllink
  = "["u:urlText"]" "("d:linkDisplayText")" {return {"type":"urlLink","url":u,"displayText":d};}

urlText
  = prefix:("https"/"http") "://" address:[^\]]+ {return prefix+"://"+address.join("");}

hyperdoclink
  = "["l:linkText"]" "("d:linkDisplayText")" {return {"type":"link","linkText":l,"displayText":d};}

linkText
  = t:[^\]]* {return t.join("").replace(/\s/g,"");}
  
linkDisplayText
  = t:[^\)]* {return t.join("");}

word
  = s:stringNoWhiteSpace {return {"type":"word",text:s};}
  
stringNoWhiteSpace
  = s:[^  \n\r\t]+ {return s.join("");}