"use strict";
var InterpreterSpecOperations={};
    
InterpreterSpecOperations.insertInitializeSubsystem=function(docObj,subsystemDisplayName,subsystemNamespace,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
   
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","InitializeSubsystem");
    commandObj.setAttribute("commandDisplayName","Initialize Subsystem");
    commandObj.setAttribute("subsystemDisplayName",subsystemDisplayName);
    commandObj.setAttribute("subsystemNamespace",subsystemNamespace);
    return commandObj;
};

InterpreterSpecOperations.insertSelectCommand=function(docObj,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","SelectCommand");
    commandObj.setAttribute("commandDisplayName","Select Command");
    return commandObj;
};

InterpreterSpecOperations.insertCommand=function(docObj,letter,name,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
   
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,letter,name");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","Command");
    commandObj.setAttribute("letter",letter);
    commandObj.setAttribute("name",name);
    commandObj.setAttribute("next",null);
    return commandObj;
};

InterpreterSpecOperations.insertCommandInput=function(docObj,toFollow,position,commands,values,valueVar){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","CommandInput");
    commandObj.setAttribute("commandDisplayName","Command Input");
    commandObj.setAttribute("commands",commands);
    commandObj.setAttribute("values",values);
    commandObj.setAttribute("valueVar",valueVar);
};

InterpreterSpecOperations.insertOutput=function(docObj,output,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,output");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","Output");
    commandObj.setAttribute("output",output);
    return commandObj;
};

InterpreterSpecOperations.insertInputForm=function(docObj,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
   
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","InputForm");
    var lastObj=commandObj.setAttribute("commandDisplayName","Show Input Form");
    lastObj=BaseSpecOperations.insertObjectAttribute(docObj,"fields",lastObj);
    metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",lastObj,"child");
    metadataObj.setAttribute("showAddresses",false);
    return commandObj;
};

// TODO: check to make sure parent is a fields attribute with an inputform object as its parent
InterpreterSpecOperations.insertFieldObj=function(docObj,inputFormObj,numObjects){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,inputFormObj);
   
    if(numObjects===undefined){
        numObjects=1;
    }
    else if(typeof(numObjects)==="string"){
        numObjects=parseInt(numObjects);
    }
    var i,fieldObj,metadataObj;
    for(i=0;i<numObjects;i++){
        fieldObj=new ObjectModels.ObjectModel();
        metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",fieldObj,"child");
        metadataObj.setAttribute("showAddresses",false);
        metadataObj.setAttribute("attributesToHide","type");
        fieldObj.setAttribute("type","fieldObj");
        fieldObj.parent=obj;
        obj.children.push(fieldObj);
    }
    return fieldObj;
};

InterpreterSpecOperations.insertExecuteFunction=function(docObj,functionName,parameters,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","ExecuteFunction");
    commandObj.setAttribute("commandDisplayName","Execute Function");
    commandObj.setAttribute("functionName",functionName);
    commandObj.setAttribute("parameters",parameters);
    return commandObj;
};

InterpreterSpecOperations.insertResetInterpreter=function(docObj,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    
    var commandObj=BaseSpecOperations.insertEmptyObject(docObj,obj,position);
    var metadataObj=BaseSpecOperations.insertObjectAttribute(docObj,"metadata",commandObj,"child");
    metadataObj.setAttribute("attributesToHide","type,commandName,commandDisplayName");
    metadataObj.setAttribute("showAddresses",false);
    commandObj.setAttribute("type","InterpreterCommand");
    commandObj.setAttribute("commandName","ResetInterpreter");
    commandObj.setAttribute("commandDisplayName","Reset Interpreter");
    return commandObj;
};

InterpreterSpecOperations.switchToBaseSubsystem=function(doc){
    BaseSpecOperations.switchSubsystem(GuiEnv.docObjs[GuiEnv.docSelected],"BaseSpecOperations");
};


InterpreterSpecOperations.changeCommand=function(docObj,obj,letter,name){
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    obj.setAttribute("letter",letter);
    obj.setAttribute("name",name);
};

InterpreterSpecOperations.changeOutput=function(docObj,obj,output){
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    obj.setAttribute("output",output);
};