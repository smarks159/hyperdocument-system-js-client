"use strict";
var DocumentModels={};

DocumentModels.DocumentViewModel=function(rootObj){
    if(rootObj!==undefined){
        this.selectedRootObj=rootObj;
    }
    else{
        this.selectedRootObj=new ObjectModels.ObjectModel();
    }
    //this.currentObj=this.selectedRootObj;
    this.showRaw=false;
    this.levelsDownLimit=1;    
    this.showLabels=false;
    this.showObjectIds=false;
    this.showStructuralAddress=true;
    
    this.viewStateVars=["showRaw","levelsDownLimit","selectedRootObj","showLabels"];
    this.dataModel=new DocumentModels.DocumentDataModel();
};

DocumentModels.DocumentViewModel.prototype.getObjLevelFromRoot=function(obj){
    return this.getObjLevelFromAncestor(this.selectedRoot,obj);    
};

DocumentModels.DocumentViewModel.prototype.getObjLevelFromAncestor=function(ancestorObj,obj){
    var level=0;
    var currentObj=obj;
    while(currentObj!==ancestorObj){
        level+=1;
        currentObj=currentObj.parent;
    }
    return level;
};

DocumentModels.DocumentViewModel.prototype.getObjLevelFromOrigin=function(obj){
    var level=0;
    var currentObj=obj;
    while(currentObj.parent!==null){
        level+=1;
        currentObj=currentObj.parent;
    }
    return level;
};

DocumentModels.DocumentViewModel.prototype.getObjIndex=function(obj){    
    var parentObj=obj.parent;
    if(parentObj===null){
        return null;
    }
    var children=this.getVisibleChildren(parentObj);
    var index=-1;    
    for(var i=0;i<children.length;i++){        
        index+=1;        
        if(children[i]===obj){
            return index;
        }
    }
    return index;
};

// gets a sibling object of the selectedRootNode based on a numeric index
DocumentModels.DocumentViewModel.prototype.getObjByIndex=function(indexToGet){ 
    var obj=this.selectedRootObj;    
    var parentObj=obj.parent;
    if(parentObj===null){
        return null;
    }
    return this.getChildByIndex(parentObj,indexToGet);    
};

DocumentModels.DocumentViewModel.prototype.getChildByIndex=function(obj,indexToGet){    
    var children=this.getVisibleChildren(obj); 
    for(var i=0;i<children.length;i++){   
        if(i===indexToGet){
            return children[i];
        }
    }
    return null;
};

DocumentModels.DocumentViewModel.prototype.hasVisibleChildren=function(obj){    
    var children=this.getVisibleChildren(obj);
    if(children.length===0){
        return false;
    }
};

// get object structural path, in the format of alternating numbers and letters starting with a number
// at the origin, ie. 1d3c
DocumentModels.DocumentViewModel.prototype.getObjStructuralPath=function(obj){
    var level=this.getObjLevelFromOrigin(obj);
    var path="";
    if(obj.parent===null){
        return "0";
    }
    var letter=false;
    // the address start at one level down from the origin and start with numbers
    if (level%2===0){
        letter=true;
    }
    var currentObj=obj;
    var index;
    while(currentObj.parent!==null){
        index=this.getObjIndex(currentObj);        
        index+=1;
        if(letter===false){
            path=index+path;
            letter=true;
        }
        else{            
            path=DocumentModels.convertIndexToLetterPath(index)+path;
            letter=false;
        }
        currentObj=currentObj.parent;
    }
    return path;
};

DocumentModels.DocumentViewModel.prototype.getObjectByAddress=function(address){
    var currentObj=this.getOriginObj();
    if(address==="0"){
        return currentObj;
    }
    
    // the address can be a label starting with a letter   
    if(address.match(/^[a-z]\w+$/i)){
        if(this.labels[address]===undefined){
            return null;
        }
        return this.getObjectByAddress(this.labels[address]);
    }
    // the address can be an object id string starting with a 0
    else if(address.match(/^0[0-9]+$/)){
        var id=parseInt(address.slice(1));
        return this.getObjectById(id);
    }
    // the address can be a structural address of alternating numbers and letters
    else{    
        var addressPath=address.split(/([0-9]+)/).filter(Boolean);
        for(var i=0;i<addressPath.length;i++){
            var index=addressPath[i];
            if(isNaN(index)){
                index=DocumentModels.convertLetterPathToIndex(index);
            }
            else{
                index=parseInt(index);
            }
            currentObj=this.getChildByIndex(currentObj,index-1);
            if(currentObj===null){
                return null;
            }
        }
    }
    return currentObj;
};

DocumentModels.DocumentViewModel.prototype.getOriginObj=function(){
    return this.selectedRootObj.getOriginObj();
};

// show the raw attributes of an object when document.showRaw is true or when
// the showRaw property of the metadata is true;
// if the object is the metadata object, then check the showRaw attribute of the object itself
// if the object is a child of the metadata object then check the showRaw attribute of the parent
DocumentModels.DocumentViewModel.prototype.showRawAttributes=function(obj){
    if(this.showRaw){
        return true;
    }
    else if(obj.getAttribute("metadata")!==null && obj.getAttribute('metadata').getAttribute('showRaw')===true){
        return true;
    }
    else if(obj.name==="metadata" && obj.getAttribute('showRaw')===true){
        return true;
    }
    else if(obj.parent!==null && obj.parent.name==="metadata" && obj.parent.getAttribute('showRaw')===true){
        return true;
    }
    else{
        return false;
    }
};

DocumentModels.DocumentViewModel.prototype.isObjectVisible=function(attrObj){
    if(this.showRawAttributes(attrObj.parent)){
        return true;
    }
    if(attrObj.name==="metadata" || attrObj.parent.name==="metadata"){
        return false;
    }
    if(attrObj.type==="Attribute"){
        var attributesToHide=[];
        var metadata=attrObj.parent.getAttribute('metadata');
        if(metadata!==null && metadata.getAttribute("attributesToHide")!==null){
            attributesToHide=metadata.getAttribute("attributesToHide").split(",");
        }
        if(attributesToHide.indexOf(attrObj.name)!==-1){
            return false;
        }
    }
    
    return true;
};

DocumentModels.DocumentViewModel.prototype.getVisibleChildren=function(obj){
    var visibleChildren=[],childObj;
    for(var i=0;i<obj.children.length;i++){
        childObj=obj.children[i];  
        if(this.isObjectVisible(childObj)){
            visibleChildren.push(childObj);
        }
    }
    return visibleChildren;
};

DocumentModels.DocumentViewModel.prototype.clone=function(obj){
    if(obj===undefined){
        obj=this.selectedRootObj;
    }
    var newDoc=new DocumentModels.DocumentViewModel();
    var jsonStr=BaseSpecOperations.convertSpecObjToJSONString(obj);
    newDoc.selectedRootObj=BaseSpecOperations.convertJSONToSpecObj(JSON.parse(jsonStr));
    return newDoc;
    
};

// This functions takes an object id as an integer and returns an object associated with that integer.
DocumentModels.DocumentViewModel.prototype.getObjectById=function(objId){
    var i,child,result;
    var rootObject=this.getOriginObj();
    for(i=0;i<rootObject.children.length;i++){
        child=rootObject.children[i];
        if(child.docId===objId){
            return child;
        }
        result=this.getObjectByIdRecursive(child,objId);
        if(result!==null){
            return result;
        }
    }
    return null;
};

DocumentModels.DocumentViewModel.prototype.getObjectByIdRecursive=function(currentObj,objId){
    var i,child,result;    
    for(i=0;i<currentObj.children.length;i++){
        child=currentObj.children[i];
        if(child.docId===objId){
            return child;
        }
        result=this.getObjectByIdRecursive(child,objId);
        if(result!==null){
            return result;
        }
    }
    return null;
};

DocumentModels.DocumentViewModel.prototype.getViewState=function(){
    var key;
    var viewState={};
    for(var i=0;i<this.viewStateVars.length;i++){
        key=this.viewStateVars[i];
        viewState[key]=this[key];
    }
    return viewState;
};

DocumentModels.DocumentViewModel.prototype.setViewState=function(viewState){
    var key;
    for(var i=0;i<this.viewStateVars.length;i++){
        key=this.viewStateVars[i];
        this[key]=viewState[key];
    }
};

DocumentModels.convertIndexToLetterPath=function(index){
    var letterPath="";
    while(index>0){
        index--;
        letterPath=String.fromCharCode(65+index%26)+letterPath;
        index=Math.floor(index/26);
    }
    return letterPath;
};

DocumentModels.convertLetterPathToIndex=function(letters){
    var letterArray=letters.split(/([A-Z]|[a-z])/).filter(Boolean);
    var index=0;
    var num,letter;
    for (var i=letterArray.length-1; i>=0;i--){
        letter=letterArray[i].toUpperCase();
        num=letter.charCodeAt(0)-64;
        index=index+(num*Math.pow(26,letterArray.length-(i+1)));
    }
    return index;
};


Object.defineProperty(DocumentModels.DocumentViewModel.prototype,"labels",{
    get: function() {return this.dataModel.labels;},
    set: function(labels) {return this.dataModel.labels=labels;}
});

Object.defineProperty(DocumentModels.DocumentViewModel.prototype,"filename",{
    get: function() {return this.dataModel.filename;},
    set: function(filename) {return this.dataModel.filename=filename;}
});

DocumentModels.DocumentDataModel=function(){
    this.filename="";
    this.labels={};
    this.objectCounter=0;
};