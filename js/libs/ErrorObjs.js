"use strict";
var SpecSystemError=function(message){
    var error=new Error();
    error.name = "SpecSystemError";
    error.message = message;
    return error;
};
