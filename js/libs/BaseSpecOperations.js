"use strict";
var BaseSpecOperations={};

BaseSpecOperations.insertStringAttribute=function(docObj,name,value,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);        
    var newObj=new ObjectModels.AttributeModel(value,name);
    newObj.docId=BaseSpecOperations.generateDocId(docObj);
    BaseSpecOperations.insertAtPosition(docObj,obj,newObj,position);
    return newObj;
};

BaseSpecOperations.insertBooleanAttribute=function(docObj,name,value,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);     
    if(typeof(value)==="string"){
        var lowerVal=value.toLowerCase();
        if(lowerVal==='true'){
            value=true;
        }
        else{
            value=false;
        }
    }
    var newObj=new ObjectModels.AttributeModel(value,name);
    newObj.docId=BaseSpecOperations.generateDocId(docObj);
    BaseSpecOperations.insertAtPosition(docObj,obj,newObj,position);
    return newObj;
};

BaseSpecOperations.insertObjectAttribute=function(docObj,name,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    var newObj=new ObjectModels.AttributeModel(null,name);
    newObj.docId=BaseSpecOperations.generateDocId(docObj);
    BaseSpecOperations.insertAtPosition(docObj,obj,newObj,position);
    return newObj;
};

BaseSpecOperations.insertEmptyObject=function(docObj,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    var newObj=new ObjectModels.ObjectModel();
    newObj.docId=BaseSpecOperations.generateDocId(docObj);
    BaseSpecOperations.insertAtPosition(docObj,obj,newObj,position);
    return newObj;
};

BaseSpecOperations.insertObject=function(docObj,type,toFollow,position){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,toFollow);
    var newObj=new ObjectModels.ObjectModel();
    newObj.docId=BaseSpecOperations.generateDocId(docObj);
    var metadata= BaseSpecOperations.insertObjectAttribute(docObj,"metadata",newObj,"child");
    metadata.setAttribute("attributesToHide","type");
    newObj.setAttribute("type",type);
    BaseSpecOperations.insertAtPosition(docObj,obj,newObj,position);
    return newObj;
};

BaseSpecOperations.insertObjectGUI=function(docObj,type,name,toFollow,position){
    var newObj;
    if(name===""){
        newObj=BaseSpecOperations.insertObject(docObj,type,toFollow,position);
    }
    else{
        newObj=BaseSpecOperations.insertObjectAttribute(docObj,name,toFollow,position);
        var metadata= BaseSpecOperations.insertObjectAttribute(docObj,"metadata",newObj,"child");
        metadata.setAttribute("attributesToHide","type");
        newObj.setAttribute("type",type);
    }
    return newObj;
};


BaseSpecOperations.insertPlainText=function(docObj,text,toFollow,position){
    var newTextObj=BaseSpecOperations.insertObject(docObj,"plainText",toFollow,position); 
    var metadataObj= newTextObj.getAttribute("metadata");
    metadataObj.setAttribute("attributesToHide","text,dateCreated,dateLastModified,type");
    newTextObj.setAttribute("text",text);
    newTextObj.setAttribute("dateCreated",new Date());
    newTextObj.setAttribute("dateLastModified",new Date());
};

BaseSpecOperations.insertAtPosition=function(docObj,obj,newObj,position){
    // insert new object as first child
    if(position==="child"){
        newObj.parent=obj;
        obj.children.unshift(newObj);
    }
    // make the new object the parent of the selected object
    else if(position==="parent"){
        
    }
    // insert the object as the successor to the current obj., unless the origin node is selected
    else{
        if(obj.parent===null){
            obj.children.unshift(newObj);
            newObj.parent=obj;
        }
        else{
            var index=docObj.getObjIndex(obj);
            newObj.parent=obj.parent;
            obj.parent.children.splice(index+1,0,newObj);
        }
    };
};

BaseSpecOperations.jumpToSuccessor=function(docObj){
    var obj=docObj.selectedRootObj;
    if(obj.parent!==null){
        var index;
        index=docObj.getObjIndex(obj);
        index+=1;
        var newObj;
        newObj=docObj.getObjByIndex(index);
        if(newObj!==null){
            docObj.selectedRootObj=newObj; 
            return newObj;
        }
        else{
            throw SpecSystemError("There is no successor to jump to");
        }
    }
    throw SpecSystemError("Cannot jump to successor on the root node");
};

BaseSpecOperations.jumpToPredecessor=function(docObj){
    var obj=docObj.selectedRootObj;
    if(obj.parent!==null){
        var index;
        index=docObj.getObjIndex(obj);
        index-=1;
        var newObj;
        newObj=docObj.getObjByIndex(index);
        if(newObj!==null){
            docObj.selectedRootObj=newObj;
            return newObj;
        }
        else{
            throw SpecSystemError("There is no predecessor to jump to");
        }
    }
    throw SpecSystemError("Cannot jump to predecessor on the root node");
};

BaseSpecOperations.jumpToSuccessorNoError=function(docObj){
    var obj=docObj.selectedRootObj;
    if(obj.parent!==null){
        var index;
        index=docObj.getObjIndex(obj);
        index+=1;
        var newObj;
        newObj=docObj.getObjByIndex(index);
        if(newObj!==null){
            docObj.selectedRootObj=newObj;
        }
        return newObj;
    }
    throw SpecSystemError("Cannot jump to successor on the root node");
};

BaseSpecOperations.jumpToElement=function(docObj,obj){
    var origAddress=obj;
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    if(obj===null){
        throw SpecSystemError("The address "+origAddress+" does not exist");
    }
    if(obj!==null || obj!==undefined){
        docObj.selectedRootObj=obj;
    }
    if(!(obj instanceof ObjectModels.ObjectModel)){
        throw SpecSystemError("Must jump to either an address or ObjectModel");
    }
    return obj;
};

BaseSpecOperations.jumpDown=function(docObj){
    var obj=docObj.selectedRootObj;
    var childObj=docObj.getChildByIndex(obj,0);
    if(childObj!==null){
        docObj.selectedRootObj=childObj;
        return childObj;
    }
    else{
        throw SpecSystemError("There is no node to jump down to.");
    }
};

BaseSpecOperations.jumpUp=function(docObj){
    var obj=docObj.selectedRootObj;
    if(obj.parent!==null){
        docObj.selectedRootObj=obj.parent;
    }
};

BaseSpecOperations.jumpDownNoError=function(docObj){
    var obj=docObj.selectedRootObj;
    var childObj=docObj.getChildByIndex(obj);
    if(childObj!==null){
        docObj.selectedRootObj=childObj;
        return childObj;
    }
    else{
        return null;
    }
};

BaseSpecOperations.findChildrenByAttributes=function(docObj,attrs){
    var matches=[];
    var origPosition=docObj.selectedRootObj;
    var childObj=BaseSpecOperations.jumpDown(docObj);    
    var matchFound;
    while(childObj!==null){        
        matchFound=true;
        $.each(attrs,function(key,value){
            if(childObj.getAttribute(key)!==value){
                matchFound=false;
                return;
            }
        });
        if(matchFound){
            matches.push(childObj);
        }
        childObj=BaseSpecOperations.jumpToSuccessorNoError(docObj); 
    }
    docObj.selectedRootObj=origPosition;
    return matches;
};

BaseSpecOperations.saveFile=function(docObj,filename){  
    var jsonString=BaseSpecOperations.convertDocObjToJSONString(docObj);    
    docObj.filename=filename;
    filename="fileDB/"+filename;
    docObj.pathname=filename;
    var url="/webservice2/saveJSONFile";
    var error=false,errorMsg;
    $.ajax({
        url:url,
        type:"POST",
        data:{filename:filename,data:jsonString},
        error:function(data,textStatus,jqXHR){   
            jsonData=JSON.parse(data.responseText);
            error=true;
            errorMsg=jsonData.error.message;
        },
        async:false
    });
    
    if(error){
        throw SpecSystemError(errorMsg);
    }
};

BaseSpecOperations.loadFile=function(doc,filename){   
    var origState=doc.getViewState();
    var origFileName=doc.filename;
    var specDoc=BaseSpecOperations.loadFromJSON(filename);
    specDoc.filename=filename;
    GuiEnv.docObjs[GuiEnv.docSelected]=specDoc;    
    // if this file is already loaded in any currently open windows, update the data in the windows to keep them in sync
    // while preserving the view state of the existing windows.
    $.each(GuiEnv.docObjs,function(key,value){
        if(GuiEnv.docObjs[key].filename===filename){ 
            var currentState=GuiEnv.docObjs[key].getViewState();            
            BaseSpecOperations.copyDocument(null,GuiEnv.docSelected,key); 
            
            if(key!==GuiEnv.docSelected){
                GuiEnv.docObjs[key].setViewState(currentState);
            }
            // Handles the case when reloading the same file. Cannot use the currentState because
            // the selected window already contains the new document, with a new view state and filename. 
            // To preserve the state when reloading the same document we must get the view state of the 
            // document before loading the new file.    
            else if(origFileName===filename){
                GuiEnv.docObjs[key].setViewState(origState);
            }
        }        
    });
};

BaseSpecOperations.convertDocObjToJSONString=function(docObj){
    var currentObj=docObj.selectedRootObj;
    // when serializing a document the rootSelectedObject is always the root object, this is
    // necessary to save the entire document because the document view model and the data model are
    // currently mixed together this should be fixed when the view model and data model are seperated out
    BaseSpecOperations.jumpToElement(docObj,"0");
    var jsonData=BaseSpecOperations.convertSpecObjToJSONString(docObj);
    BaseSpecOperations.jumpToElement(docObj,currentObj); 
    return jsonData;
};

BaseSpecOperations.convertSpecObjToJSONString=function(specObj){
    var jsonData=JSON.stringify(specObj,
        function(key,value){
            if(key==="parent"){
                return undefined;
            }
            else if(key==="children" && value.length===0){
                return undefined;
            }
            else{
                return value;
            }
            
        });
   return jsonData;
};

BaseSpecOperations.loadFromJSON=function(fileName){
    var url="/webservice2/loadJSON";
    var jsonData=null;
    var error=false,errorMsg;
    
    fileName="fileDB/"+fileName+".json";
    $.ajax({
        url:url,
        type:"POST",
        data:{fileName:fileName},
        success:function(data,textStatus,jqXHR){
            jsonData=data;
        },
        error:function(data,textStatus,jqXHR){   
            jsonData=data;
            error=true;
            errorMsg=jsonData.error.message;
        },
        async:false
    });
    
    if(error){
        throw SpecSystemError(errorMsg);
    }
    else{      
        return BaseSpecOperations.convertJSONToSpecDoc(jsonData); 
    }
   
};

BaseSpecOperations.convertJSONToSpecDoc=function(jsonData){
    var specDoc=new DocumentModels.DocumentViewModel();
    $.each(jsonData,function(key,value){
        if(key!=="selectedRootObj"){
            specDoc[key]=jsonData[key];
        }
    });
    specDoc.selectedRootObj=BaseSpecOperations.convertJSONToSpecObj(jsonData.selectedRootObj);
    // convert old objectCounter format to new one.
    if(specDoc.selectedRootObj.objectCounter!==undefined && specDoc.selectedRootObj.objectCounter>specDoc.dataModel.objectCounter){
        specDoc.dataModel.objectCounter=specDoc.selectedRootObj.objectCounter;
        delete specDoc.selectedRootObj.objectCounter;
    }
    BaseSpecOperations.addDocIds(specDoc,specDoc.selectedRootObj);
    return specDoc;
};
BaseSpecOperations.convertJSONToSpecObj=function(currentObjData){
        // create an object or attribute model based on the type property
        var objCls=currentObjData["type"]+"Model";
        var newObj=new ObjectModels[objCls]();
        
        // set all json attribute to object attributes, except for children
        $.each(currentObjData,function(key,value){
            if(key!=="children"){
                newObj[key]=value;
            }
        });
        
        // for each child create an object, set its parent to the current object and add the child object
        // to the children array.
        var childObj;
        if(currentObjData.children!==undefined){
            $.each(currentObjData.children,function(i,childData){
                childObj=BaseSpecOperations.convertJSONToSpecObj(childData);
                childObj.parent=newObj;
                newObj.children.push(childObj);
            });
        }
        return newObj;
};

// If the docId does not exist for an object add it (needed to convert old documents without docIds)
BaseSpecOperations.addDocIds=function(docObj,obj){
    // if the node does not have a docId create a new one (needed to convert old documents without
    // docIds to new documents.
    // also if there are duplicateIds create a new id.
    // The origin node does not get a docId.
    
    var uniqueIds={};
    if(obj.docId===null && obj.parent!==null){
        obj.docId=BaseSpecOperations.generateDocId(docObj);        
    }
    uniqueIds[obj.docId]=true;
    $.each(obj.children,function(i,childObj){
         BaseSpecOperations.addDocIdsRecursive(docObj,childObj,uniqueIds);
    });
};

BaseSpecOperations.addDocIdsRecursive=function(docObj,obj,uniqueIds){
    if(obj.docId===null && obj.parent!==null){
        obj.docId=BaseSpecOperations.generateDocId(docObj);        
    } 
    else if(uniqueIds[obj.docId]!==undefined){
        obj.docId=BaseSpecOperations.generateDocId(docObj);
    }
    uniqueIds[obj.docId]=true;
    $.each(obj.children,function(i,childObj){
         BaseSpecOperations.addDocIdsRecursive(docObj,childObj,uniqueIds);
    });
};

BaseSpecOperations.generateNewDocIds=function(docObj,obj){
    obj.docId=BaseSpecOperations.generateDocId(docObj); 
    $.each(obj.children,function(i,childObj){
         BaseSpecOperations.generateNewDocIds(docObj,childObj);
    });
};

BaseSpecOperations.switchSubsystem=function(docObj,subsystem){
    var interpreterSpec=BaseSpecOperations.loadFromJSON("interpreterspecs/"+subsystem);
    interpreter.spec=interpreterSpec;
    interpreter.ResetInterpreter();
    RenderDocument.renderSubsystemLabel(interpreter);
    // execute action goes to the next interpreter action after executing the function, this
    // prevents the next action from being execute.
    // TODO: Handle this a better way.
    interpreter.resetCalled=true;
};

BaseSpecOperations.switchToInterpreterSpecSubsystem=function(docObj){
    BaseSpecOperations.switchSubsystem(docObj,"InterpreterSpecOperations");
};

BaseSpecOperations.deleteBranch=function(docObj,obj){
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);    
    var i;    
    if(obj.parent!==null){
        if(obj===docObj.selectedRootObj){
            BaseSpecOperations.jumpUp(docObj);
        }
        var parentObj=obj.parent;
        var childObj;
        for(i=0;i<parentObj.children.length;i++){
            childObj=parentObj.children[i];
            if(obj===childObj){
                parentObj.children.splice(i,1);
                obj.parent=null;
                break;
            }
        }
    }
    else if(obj!==null) {
        BaseSpecOperations.deleteDocument(docObj,GuiEnv.docSelected);
    }
    else{
        throw SpecSystemError("The Object Passed in does not exist in the document");
    }
};

BaseSpecOperations.copyBranch=function(docObj,sourceObj,destinationObj,position){
    sourceObj=BaseSpecOperations.getObjectFromAddress(docObj,sourceObj);
    destinationObj=BaseSpecOperations.getObjectFromAddress(docObj,destinationObj);    
    var jsonData=BaseSpecOperations.convertSpecObjToJSONString(sourceObj);
    var objCopy=BaseSpecOperations.convertJSONToSpecObj(JSON.parse(jsonData));   
    BaseSpecOperations.generateNewDocIds(docObj,objCopy);
    BaseSpecOperations.insertAtPosition(docObj,destinationObj,objCopy,position);
    return objCopy;
};

BaseSpecOperations.copyBranchNoGenIds=function(docObj,sourceObj,destinationObj,position){
    sourceObj=BaseSpecOperations.getObjectFromAddress(docObj,sourceObj);
    destinationObj=BaseSpecOperations.getObjectFromAddress(docObj,destinationObj);    
    var jsonData=BaseSpecOperations.convertSpecObjToJSONString(sourceObj);
    var objCopy=BaseSpecOperations.convertJSONToSpecObj(JSON.parse(jsonData));
    BaseSpecOperations.insertAtPosition(docObj,destinationObj,objCopy,position);
    return objCopy;
};

BaseSpecOperations.copyRange=function(docObj,startRangeObj,endRangeObj,destinationObj,position){
    startRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,startRangeObj);
    endRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,endRangeObj);
    destinationObj=BaseSpecOperations.getObjectFromAddress(docObj,destinationObj);    
    
    if(startRangeObj.parent!==endRangeObj.parent){
        throw new Error("The start and end of the range must be on the same level");
    }
    
    var startIndex=docObj.getObjIndex(startRangeObj);
    var endIndex=docObj.getObjIndex(endRangeObj);
    
    var docTemp=new DocumentModels.DocumentViewModel(startRangeObj); 
    var objsToCopy=[];
    
    // handle case where user selects range in reverse
    if(startIndex>endIndex){
        var tmp=startIndex;
        startIndex=endIndex;
        endIndex=tmp;
    }
    
    // must get all the objects by index first, before copying. This is because copying the object
    // to the same branch changes the index.
    for(var i=startIndex;i<=endIndex;i++){
        objsToCopy.push(docTemp.getObjByIndex(i));        
    }
    
    for(var j=0;j<objsToCopy.length;j++){
        destinationObj=BaseSpecOperations.copyBranch(docObj,objsToCopy[j],destinationObj,position);        
        if(j===0){
            position="successor";
        }
    }
};

BaseSpecOperations.moveRange=function(docObj,startRangeObj,endRangeObj,destinationObj,position){
    startRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,startRangeObj);
    endRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,endRangeObj);
    destinationObj=BaseSpecOperations.getObjectFromAddress(docObj,destinationObj);
    
    if(startRangeObj.parent!==endRangeObj.parent){
        throw new Error("The start and end of the range must be on the same level");
    }
    
    var startIndex=docObj.getObjIndex(startRangeObj);
    var endIndex=docObj.getObjIndex(endRangeObj);
    var docTemp=new DocumentModels.DocumentViewModel(startRangeObj);
    var objsToMove=[];
    
    // handle case where user selects range in reverse
    if(startIndex>endIndex){
        var tmp=startIndex;
        startIndex=endIndex;
        endIndex=tmp;
    }
    
    // must get all the objects by index first, before moving. This is because moving the object changes the indexes of the branch.
    for(var i=startIndex;i<=endIndex;i++){
        objsToMove.push(docTemp.getObjByIndex(i));        
    }
    
    for(var j=0;j<objsToMove.length;j++){
        destinationObj=BaseSpecOperations.moveBranch(docObj,objsToMove[j],destinationObj,position);        
        if(j===0){
            position="successor";
        }
    }
};

BaseSpecOperations.deleteRange=function(docObj,startRangeObj,endRangeObj){
    startRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,startRangeObj);
    endRangeObj=BaseSpecOperations.getObjectFromAddress(docObj,endRangeObj);
    
    if(startRangeObj.parent!==endRangeObj.parent){
        throw new Error("The start and end of the range must be on the same level");
    }
    
    var startIndex=docObj.getObjIndex(startRangeObj);
    var endIndex=docObj.getObjIndex(endRangeObj);
    var docTemp=new DocumentModels.DocumentViewModel(startRangeObj);
    var objsToDelete=[];
    
    // handle case where user selects range in reverse
    if(startIndex>endIndex){
        var tmp=startIndex;
        startIndex=endIndex;
        endIndex=tmp;
    }
    
    // must get all the objects by index first, before deleting. This is because deleting the object changes the indexes of the branch.
    for(var i=startIndex;i<=endIndex;i++){
        objsToDelete.push(docTemp.getObjByIndex(i));        
    }
    
    for(var j=0;j<objsToDelete.length;j++){
        BaseSpecOperations.deleteBranch(docObj,objsToDelete[j]);                
    }
};

BaseSpecOperations.copyDocument=function(docObj,sourceWindow,targetWindow){
    if(sourceWindow===targetWindow){
        // if the source and target window are the same, do nothing.
        return;
    }    
    var sourceView=GuiEnv.docObjs[sourceWindow];
    var sourceViewState=sourceView.getViewState();
    if(sourceView.filename===""){
        throw new SpecSystemError("cannot copy a document that has not been saved to a file");
    }
    var targetView=new DocumentModels.DocumentViewModel(sourceView.selectedRootObj);
    targetView.setViewState(sourceViewState);
    targetView.dataModel=sourceView.dataModel;    
    GuiEnv.docObjs[targetWindow]=targetView;
};

BaseSpecOperations.moveDocument=function(docObj,sourceWindow,targetWindow){
    if(sourceWindow===targetWindow){
        throw new SpecSystemError("The source window and the target window cannot be the same");
    }
    var sourceView=GuiEnv.docObjs[sourceWindow];
    if(sourceView.filename===""){
        throw new SpecSystemError("cannot move a document that has not been saved to a file");
    }
    BaseSpecOperations.copyDocument(docObj,sourceWindow,targetWindow);
    BaseSpecOperations.deleteDocument(docObj,sourceWindow);
};

BaseSpecOperations.moveBranch=function(docObj,sourceObj,destinationObj,position){    
    var sourceAddress=sourceObj;
    sourceObj=BaseSpecOperations.getObjectFromAddress(docObj,sourceObj);
    destinationObj=BaseSpecOperations.getObjectFromAddress(docObj,destinationObj);
    var sourceDocObj=BaseSpecOperations.getDocObjFromAddress(docObj,sourceAddress);
    var newObj;
    if(BaseSpecOperations.objectsInSameDocument(sourceObj,destinationObj)){
        newObj=BaseSpecOperations.copyBranchNoGenIds(docObj,sourceObj,destinationObj,position);
    } else{
        newObj=BaseSpecOperations.copyBranch(docObj,sourceObj,destinationObj,position);
    }
    BaseSpecOperations.deleteBranch(sourceDocObj,sourceObj);    
    return newObj;
};

BaseSpecOperations.toggleViewRawDocument=function(doc){
    if(doc.showRaw===false){
        doc.showRaw=true;
    }else{
        doc.showRaw=false;
    }
};

BaseSpecOperations.toggleViewRawObject=function(docObj,obj){
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    if(obj.getAttribute("metadata")!==null){
        var metadata=obj.getAttribute('metadata');
        if(metadata.getAttribute('showRaw')!==true){
            metadata.setAttribute('showRaw',true);
        }
        else{
            metadata.setAttribute('showRaw',false);
        }
    }
};

BaseSpecOperations.viewLevelDecrease=function(doc){    
    if(doc.levelsDownLimit!==0){
        doc.levelsDownLimit-=1;
    }
};

BaseSpecOperations.viewLevelIncrease=function(doc){
    doc.levelsDownLimit+=1;
};

BaseSpecOperations.viewLevelSet=function(doc,level){
    level=parseInt(level);
    doc.levelsDownLimit=level;
};


// TODO: Check to 
BaseSpecOperations.changeStringAttribute=function(docObj,objToChange,name,value){
    objToChange=BaseSpecOperations.getObjectFromAddress(docObj,objToChange);
    if(objToChange.type!=="Attribute" || objToChange.getAttribute("type")!==null || objToChange.parent===null){
        throw new Error("changeStringAttribute only works on primative attribute values");
    }
    
    var parentObj=objToChange.parent;
    var i,childObj;
    for(i=0;i<parentObj.children.length;i++){
        childObj=parentObj.children[i];        
        if(childObj===objToChange){
            var newObj=new ObjectModels.AttributeModel(value,name);
            newObj.parent=parentObj;
            parentObj.children[i]=newObj;
            childObj.parent=null;
            break;
        }
    }
};

BaseSpecOperations.changeText=function(docObj,objToChange,value){
    objToChange=BaseSpecOperations.getObjectFromAddress(docObj,objToChange);    
    if(objToChange.getAttribute("type")!=="plainText"){
        throw new Error("changeText only works on plainText nodes");
    }
    objToChange.setAttribute("text",value);
    objToChange.setAttribute("dateLastModified",new Date());
};

BaseSpecOperations.getObjectFromAddress=function(docObj,address){
    if(typeof(address)==="string"){
        var parts=address.split(",");
        if(parts[1]!==undefined && parts[1]!==""){
            docObj=GuiEnv.docObjs[parts[1]];
        }
        return docObj.getObjectByAddress(parts[0]);
    }
    else{
        return address;
    }
};

BaseSpecOperations.getDocObjFromAddress=function(docObj,address){
    if(typeof(address)!=="string"){
        return docObj;
    }
    else{
        var parts=address.split(",");
        if(parts[1]!==undefined && parts[1]!==""){
            docObj=GuiEnv.docObjs[parts[1]];
            return docObj;
        }
        else{
            return docObj;
        }
    }
};


BaseSpecOperations.showWindow=function(docObj,windowName){ 
    if(["left","right","up","down"].indexOf(windowName)===-1){
        throw SpecSystemError("The windowName "+windowName+ " is not supported");
    }
    $("[data-windowId="+windowName+"]").css("display","block");    
};


BaseSpecOperations.hideWindow=function(docObj,windowName){
    if(windowName==="center"){
        throw SpecSystemError("The center window cannot be deleted");
    }
    else if(["left","right","up","down"].indexOf(windowName)===-1){
        throw SpecSystemError("The windowName "+windowName+ " is not supported");
    }
    $("[data-windowId="+windowName+"]").css("display","none");
    // If the window that is current selected is closed switch to the center window.
    if(GuiEnv.docSelected===windowName){
        $("[data-windowId=center]").mousedown();
    }
};

BaseSpecOperations.objectsInSameDocument=function(obj1,obj2){
    if(obj1.getOriginObj()===obj2.getOriginObj()){
        return true;
    }
    else{
        return false;
    }
};

// objectCounter is used to create a unique id for every object on the page when it is created.
BaseSpecOperations.generateDocId=function(docObj){
    docObj.dataModel.objectCounter++;
    return docObj.dataModel.objectCounter;
};

BaseSpecOperations.deleteDocument=function(docObj,window){
    GuiEnv.docObjs[window]=new DocumentModels.DocumentViewModel(); ;
};

BaseSpecOperations.insertLabel=function(docObj,obj,labelName){
    if(docObj.labels[labelName]!==undefined){
        throw SpecSystemError("The label '"+labelName+"'"+" already exists in the document");
    }
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);        
    if(obj.label!==null){
        throw SpecSystemError("The object already has a label, each object can only have one label");
    }
    obj.label=labelName;
    docObj.labels[labelName]="0"+obj.docId;
};

BaseSpecOperations.deleteLabel=function(docObj,obj){
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    var label=obj.label;
    obj.label=null;
    delete docObj.labels[label];
};

BaseSpecOperations.moveLabel=function(docObj,sourceObj,targetObj){
    sourceObj=BaseSpecOperations.getObjectFromAddress(docObj,sourceObj);
    targetObj=BaseSpecOperations.getObjectFromAddress(docObj,targetObj);
    var sourceLabel=sourceObj.label;
    targetObj.label=sourceLabel;
    sourceObj.label=null;
    delete docObj.labels[sourceLabel];
    docObj.labels[targetObj.label]="0"+targetObj.docId;
};

BaseSpecOperations.changeLabel=function(docObj,obj,newLabelName){
    if(docObj.labels[newLabelName]!==undefined){
        throw SpecSystemError("The label '"+newLabelName+"'"+" already exists in the document");
    }    
    obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);    
    delete docObj.labels[obj.label];
    obj.label=newLabelName;
    docObj.labels[newLabelName]="0"+obj.docId;
};

BaseSpecOperations.jumpToLabel=function(docObj,labelName){
    if(docObj.labels[labelName]===undefined){
        throw SpecSystemError("The label '"+labelName+"'"+" does not exist in the document");
    }  
    BaseSpecOperations.jumpToElement(docObj,docObj.labels[labelName]);
};


BaseSpecOperations.addToJumpHistory=function(docObj,selectedWindow){
    if(selectedWindow===undefined){
        selectedWindow=GuiEnv.docSelected;
    }
    GuiEnv.jumpHistory[selectedWindow].setCurrent(new InMemoryHistory.jumpHistoryObj(docObj.selectedRootObj,docObj.filename));
};

BaseSpecOperations.jumpBack=function(){
    var selectedWindow=GuiEnv.docSelected;
    var histObj=GuiEnv.jumpHistory[selectedWindow].goBack();
    if(histObj!==null){
        var docDOMel=$("#doc"+selectedWindow);
        BaseSpecOperations.jumpToElement(GuiEnv.docObjs[selectedWindow],histObj.selectedRootObj);
        GuiEnv.docObjs[selectedWindow].filename=histObj.fileName;
    }
};

BaseSpecOperations.jumpFoward=function(){
    var selectedWindow=GuiEnv.docSelected;
    var histObj=GuiEnv.jumpHistory[selectedWindow].goFoward();
    if(histObj!==null){
        var docDOMel=$("#doc"+selectedWindow);
        BaseSpecOperations.jumpToElement(GuiEnv.docObjs[selectedWindow],histObj.selectedRootObj);
        GuiEnv.docObjs[selectedWindow].filename=histObj.fileName;
    }
};

BaseSpecOperations.jumpToLink=function(docObj,link,targetWindow){
    var sourceWindow,address,fileName; 
    var linkSplit=link.split(';');
    sourceWindow=linkSplit[1].trim();
    link=linkSplit[0].trim();    
    // remove < > delimiters from links    
    address=link.slice(1,-1);
    address=address.trim().split(',');
    // an address with not commas is an interdocument link, so copy the current document to the new target window
    if (address.length===1){
        address=address[0].trim();
        BaseSpecOperations.copyDocument(docObj,sourceWindow,targetWindow);
        docObj=GuiEnv.docObjs[targetWindow];
    } else if(address.length===2){
        fileName=address[0].trim();
        address=address[1].trim();
        BaseSpecOperations.loadFile(docObj,fileName);
        docObj=GuiEnv.docObjs[targetWindow];
    }    
    BaseSpecOperations.jumpToElement(docObj,address);    
};

BaseSpecOperations.toggleTextBold=function(docObj,obj){
    var obj=BaseSpecOperations.getObjectFromAddress(docObj,obj);
    if(obj.getAttribute("type")!=="plainText"){
        throw SpecSystemError("only text object can be bolded");
    }
    if(obj.bold===true){
        obj.bold=false;
    } else{
        obj.bold=true;
    }
};

BaseSpecOperations.toggleViewLabels=function(docObj){
    if(docObj.showLabels===true){
        docObj.showLabels=false;
    } else{
        docObj.showLabels=true;
    }
};

BaseSpecOperations.toggleViewObjectIds=function(docObj){
    if(docObj.showObjectIds===true){
        docObj.showObjectIds=false;
    } else{
        docObj.showObjectIds=true;
    }
};

BaseSpecOperations.toggleViewStructuralAddress=function(docObj){
    if(docObj.showStructuralAddress===true){
        docObj.showStructuralAddress=false;
    } else{
        docObj.showStructuralAddress=true;
    }
};

BaseSpecOperations.insertLinkAfterWord=function(docObj,objToLink,linkText,wordToInsertAfter){
    var objToLinkDoc=BaseSpecOperations.getDocObjFromAddress(docObj,objToLink);
    var linkAddress = objToLinkDoc.filename + "," + "0"+ BaseSpecOperations.getObjectFromAddress(docObj,objToLink).docId;
    var link = "["+linkAddress+"]"+"("+linkText+")";
    var selectedWord=$(".selected.word").add(".selected.hyperdoclink");
    var targetTextObj=docObj.getObjectByAddress(selectedWord.closest("[data-structuralpath]").attr("data-structuralpath"));
    var text = targetTextObj.getAttribute("text").split(" ");
    text.splice(selectedWord.data("elementindex")+1,0,link);
    text = text.join(" ");
    targetTextObj.setAttribute("text",text);
};

BaseSpecOperations.insertLinkAfterObject=function(docObj,objToLink,linkText,position,objToInsertAfter){
    var objToLinkDoc=BaseSpecOperations.getDocObjFromAddress(docObj,objToLink);
    var linkAddress = objToLinkDoc.filename + "," + "0" + BaseSpecOperations.getObjectFromAddress(docObj,objToLink).docId;
    var link = "["+linkAddress+"]"+"("+linkText+")";
    BaseSpecOperations.insertPlainText(docObj,link,objToInsertAfter,position);
};

BaseSpecOperations.resetScroll=function(docObj){
    $("#doc"+GuiEnv.docSelected).parent().scrollTop(0);
    $("#doc"+GuiEnv.docSelected).parent().scrollLeft(0);
};