"use strict";
var documentDataToViewMapper=function(){
    this.dataToViewMap=[];    
};

//var documentView=function(){
//    this.objectTree=null;    
//    this.dataToViewMapper=new documentDataToViewMapper();
//    this.depthLimit=null;
//    this.startDataNode=null;
//    this.documentEl=null;
//};
//
//documentView.prototype.buildObjects=function(docObj){
//    this.startDataNode=docObj.selectedRootObj;
//    this.depthLimit=docObj.levelsDownLimit;
//    this.objectTree=this.buildObjectRecursive(docObj,docObj.selectedRootObj);
//    docObj.selectRootObj=this.startDataNode;
//};
//
//documentView.prototype.buildObjectRecursive=function(docObj,dataObj){
//    var type;
//    if(dataObj.getAttribute("type")!==null && docObj.showRawAttributes(dataObj)===false){
//        type=dataObj.getAttribute("type");
//    }
//    // only the two primatives object and attribute have type hardcoded in the object
//    else{
//        type=dataObj.type;
//    }
//    var viewObj=new window[type+"View"];
//    viewObj.build(dataObj);
//    if(this.buildChildren(docObj,dataObj)){
//        $.each(dataObj.children,function(i,child){
//            viewObj.addChild(this.buildObjectRecursive(docObj,child));
//        });
//    }    
//};
//
//documentView.prototype.buildChildren=function(docObj,dataObj){
//    if(docObj.getObjLevelFromAncestor(this.startingNode,dataObj)>this.depthLimit ||
//       docObj.hasVisibleChildren(dataObj)===false){
//        return false;
//    }
//    else{
//        return true;
//    }
//};
//
//documentView.prototype.renderView=function(){
//    
//};

var AddressView=function(){
    this.renderAddress=true;
    this.renderLabel=false;
    this.renderObjectId=false;
    this.renderStructuralAddress=true;
    this.label=null;
    this.structuralPath=null;
    this.htmlEl=null;
};

AddressView.prototype.build=function(docObj,dataObj){
    if(dataObj.parent!==null){
        var metadata=dataObj.parent.getAttribute('metadata');
        if(metadata!==null){
            if(metadata.getAttribute("showAddresses")===false){
                this.renderAddress=false;
            }
        }
    }
    // if the object addresses are hidden, hide the metadata addresses as well.
    if((dataObj.name==="metadata" && dataObj.getAttribute("showAddresses")===false) ||
       (dataObj.parent!==null && dataObj.parent.name==="metadata" && dataObj.parent.getAttribute("showAddresses")===false)){
        this.renderAddress=false;
    }
    this.structuralPath=docObj.getObjStructuralPath(dataObj);    
    this.renderLabel=docObj.showLabels;
    this.label=dataObj.label;
    this.renderObjectId=docObj.showObjectIds;
    this.renderStructuralAddress=docObj.showStructuralAddress;
    this.docId=dataObj.docId;
    
};

AddressView.prototype.render=function(){
    this.htmlEl=document.createElement("div");
    this.htmlEl.setAttribute("style","display:inline-block");
    
    if(this.renderStructuralAddress){
        var structuralAddress=document.createElement("span");
        structuralAddress.setAttribute("style","font-size:0.8em;;color:green;margin-right:1em");
        structuralAddress.innerHTML=this.structuralPath;
        this.htmlEl.appendChild(structuralAddress);
    }
    
    if(this.renderLabel && this.label!==null){
        var label=document.createElement("span");
        label.setAttribute("style","font-size:0.8em;;color:green;margin-right:1em");
        label.innerHTML="("+this.label+")";
        this.htmlEl.appendChild(label);    
    }
    
    if(this.renderObjectId){
        var objId=document.createElement("span");
        objId.setAttribute("style","font-size:0.8em;;color:green;margin-right:1em");
        objId.innerHTML="0"+this.docId;
        this.htmlEl.appendChild(objId); 
    }
    
    return this.htmlEl;
};

var ObjectView=function(){
    this.type=null;
    this.addressLabel=new AddressView(); 
    this.parent=null;
    this.children=[];
    this.isRendered=false;
    this.htmlEl=null;
    this.structuralPath=null;
    this.isOrigin=false;
};

ObjectView.prototype.build=function(docObj,dataObj){
    this.type=dataObj.getAttribute("type");
    this.addressLabel.build(docObj,dataObj);
    this.structuralPath=this.addressLabel.structuralPath;
    if(dataObj.parent===null){
        this.isOrigin=true;
    }
};
ObjectView.prototype.render=function(){
    this.htmlEl=document.createElement("div");
    this.htmlEl.setAttribute("class","treeChild");
    this.htmlEl.setAttribute("data-structuralpath",this.structuralPath);
    if(this.addressLabel.renderAddress && !this.isOrigin){
        this.htmlEl.appendChild(this.addressLabel.render());
    }
    if(this.type!==null){
        var typeEl=document.createElement("span");
        typeEl.innerHTML="("+this.type+")";
        this.htmlEl.appendChild(typeEl);
    }
    return this.htmlEl;
};

var AttributeView=function(){
    this.addressLabel=new AddressView(); 
    this.parent=null;
    this.children=[];
    this.isRendered=false;
    this.htmlEl=null;
    this.structuralPath=null;
    this.name=null;
    this.value=null;
};

AttributeView.prototype.build=function(docObj,dataObj){
    this.name=dataObj.name;
    this.value=dataObj.value;
    this.addressLabel.build(docObj,dataObj);
    this.structuralPath=this.addressLabel.structuralPath;;
};

AttributeView.prototype.render=function(){
    this.htmlEl=document.createElement("div");
    if(this.addressLabel.renderAddress){
        this.htmlEl.appendChild(this.addressLabel.render());
    }    
    this.htmlEl.setAttribute("class","treeChild");
    var nameEl=document.createElement("span");
    this.htmlEl.setAttribute("data-structuralpath",this.structuralPath);
    nameEl.innerHTML=this.name;
    this.htmlEl.appendChild(nameEl);
    var seperatorEl=document.createElement("span");
    seperatorEl.innerHTML=":";
    this.htmlEl.appendChild(seperatorEl);
    var valueEl=document.createElement("span");
    valueEl.innerHTML=this.value;
    this.htmlEl.appendChild(valueEl);
    return this.htmlEl;
};

var PlainTextView=function(){
    this.addressLabel=new AddressView(); 
    this.parent=null;
    this.children=[];
    this.isRendered=false;
    this.htmlEl=null;
    this.structuralPath=null;
    this.text=null;
    this.bold=null;
};

PlainTextView.prototype.build=function(docObj,dataObj){
    this.addressLabel.build(docObj,dataObj);
    this.structuralPath=this.addressLabel.structuralPath;;
    this.text=dataObj.getAttribute('text');
    this.row="";
    this.label=dataObj.label;
    this.bold=dataObj.bold;
};

PlainTextView.prototype.render=function(){
    this.htmlEl=document.createElement("div");
    this.htmlEl.setAttribute("class","treeChild textObject");    
    this.htmlEl.setAttribute("data-structuralpath",this.structuralPath);
    this.row=document.createElement("div");
    
    var editorEl=document.createElement("span");
    editorEl.setAttribute("class","textObjectRenderer");
    var style="";
    if(this.bold===true){
        style+="font-weight:bold;";
    }
    editorEl.setAttribute("style",style);
    
    var tokens=plainTextParser.parse(this.text);    
    
    var escapeHTML=function(str){
        return str
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
    };
    
    var outtext="",token,i;    
    for(i=0;i<tokens.length;i++){
        token=tokens[i];
        if(token.type==="link"){
            outtext+="<span class='hyperdoclink' " + "data-elementIndex=" + i +" data-link='"+"<"+token.linkText+">'>";
            if(token.displayText!==""){
                outtext+=escapeHTML(token.displayText);
            } else{
                outtext+="&lt"+escapeHTML(token.linkText)+"&gt";
            }
            outtext+="</span> ";
        }
        else if(token.type==="urlLink"){
            outtext+="<a href="+token.url+" target='_blank' class='link'" + "data-elementIndex=" + i +">"+token.displayText+"</a> ";
        }
        else{
            outtext+="<span class='word' " + "data-elementIndex=" + i +">"+escapeHTML(token.text)+"</span> ";
        }
    }
    
    editorEl.innerHTML=outtext;
    if(this.addressLabel.renderAddress){
        this.row.appendChild(this.addressLabel.render());
    }    
    this.row.appendChild(editorEl);
    this.htmlEl.appendChild(this.row);
    return this.htmlEl;
};


var InterpreterCommandView=function(){
    this.commandName=null;
    this.commandDisplayName=null;
    this.letter=null;
    this.name=null;
    this.output=null;
    this.index=0;
};

 InterpreterCommandView.prototype.build=function(docObj,dataObj){
     this.structuralPath=docObj.getObjStructuralPath(dataObj);
     this.commandName=dataObj.getAttribute("commandName");
     this.commandDisplayName=dataObj.getAttribute("commandDisplayName");
     this.letter=dataObj.getAttribute("letter");
     this.name=dataObj.getAttribute("name");
     this.output=dataObj.getAttribute("output");
     this.index=docObj.getObjIndex(dataObj)+1;
 };
 
 InterpreterCommandView.prototype.render=function(){
    this.htmlEl=document.createElement("div");
    this.htmlEl.setAttribute("class","treeChild");
    this.htmlEl.setAttribute("data-structuralpath",this.structuralPath);
    var commandEl=document.createElement("span");    
    if(this.commandName==="Command"){
        commandEl.innerHTML=this.letter+":&nbsp;&nbsp;"+this.name;
    }
    else if(this.commandName==="Output"){
        commandEl.innerHTML=this.index+":&nbsp;&nbsp;output '"+this.output+"'";
    }
    else{
        commandEl.innerHTML=this.index + ":&nbsp;&nbsp;" + this.commandDisplayName;
    }
    this.htmlEl.appendChild(commandEl);
    return this.htmlEl;
 };



var RenderDocument={
        docEl:null,
        currentObjEl:null,
        initFuncs:[],
        startingNode:null,
        callResizeOk:true
};

RenderDocument.renderDocument=function(docObj,mainDiv,windowName){
    if (mainDiv===undefined){
        RenderDocument.docEl=document.createElement("div");
    }
    else{
        RenderDocument.docEl=mainDiv;
    }
    if(windowName===null){
        windowName=GuiEnv.docSelected;
    }
    // In the center document the documentOut class must be placed in the outer row div to get proper scrolling,
    // that includes the star element.
    if(windowName!=="center"){
        RenderDocument.docEl.setAttribute("class","documentOut");
    }
    
//    var docView=new documentView();
//    docView.buildObjects(docObj);
    
    RenderDocument.startingNode=docObj.selectedRootObj;
    RenderDocument.currentObjEl=RenderDocument.docEl;
    RenderDocument.renderDocumentRecursive(docObj);
    docObj.selectedRootObj=RenderDocument.startingNode;
    RenderDocument.docEl.setAttribute("id","doc"+windowName);
    $("#doc"+windowName).replaceWith(RenderDocument.docEl);
    return RenderDocument.docEl;
};

RenderDocument.renderDocumentRecursive=function(docObj){
    var currentObj=docObj.selectedRootObj;
    var currentEl=RenderDocument.currentObjEl;
    var type;
    if(currentObj.getAttribute("type")!==null && docObj.showRawAttributes(currentObj)===false){
        type=currentObj.getAttribute("type");
    }
    // only the two primatives object and attribute have type hardcoded in the object
    else{
        type=currentObj.type;
    }
    var renderFunc="Render"+type;
    if(RenderDocument[renderFunc]===undefined){
        if(currentObj.name===null || currentObj.name===undefined){
            renderFunc="RenderObject";
        }
        else{
            renderFunc="RenderAttribute";
        }
    }
    var currentView=RenderDocument[renderFunc](currentObj,docObj);    
    if(currentObj.children!==undefined){
        if(docObj.hasVisibleChildren(currentObj)===false){
            // do nothing
        }
        else if(docObj.getObjLevelFromAncestor(RenderDocument.startingNode,currentObj)>=docObj.levelsDownLimit){
                var attrEl=document.createElement("span");
                attrEl.setAttribute("style","display:inline-block;font-size:0.8em;margin-left:0.1em;color:blue;");
                attrEl.innerHTML="*";
                if(currentView.row!==undefined && currentView.row!==""){
                    currentView.row.appendChild(attrEl);
                }else{
                    RenderDocument.currentObjEl.appendChild(attrEl);
                }
        }
        else{
            $.each(currentObj.children,function(i,child){            
                docObj.selectedRootObj=child;
                RenderDocument.renderDocumentRecursive(docObj);
                docObj.selectedRootObj=currentObj;
            });
        }
    }
    RenderDocument.currentObjEl=currentEl;
};

RenderDocument.RenderObject=function(model,docObj){    
    var view=new ObjectView();
    view.build(docObj,model);    
    var htmlEl=view.render();
    //do not indent the node under the following conditions:
    //  - the object is the origin node
    //  - the object's parent is the origin node
    //  - the object is at the top level of the selected root, but is not the child of the root node
    if(model.parent!==null &&  
        model.parent.parent!==null &&
        docObj.getObjLevelFromAncestor(RenderDocument.startingNode,model)!==0){
            var style="margin-left:2em";
            htmlEl.setAttribute("style",style); 
    }
    RenderDocument.currentObjEl.appendChild(htmlEl);
    RenderDocument.currentObjEl=htmlEl;
    return view;
};

RenderDocument.RenderAttribute=function(model,docObj){   
    if(!docObj.isObjectVisible(model)){
        return;
    }
    var view=new AttributeView();
    view.build(docObj,model);
    var htmlEl=view.render();
    if(model.parent!==null &&  
        model.parent.parent!==null &&
        docObj.getObjLevelFromAncestor(RenderDocument.startingNode,model)!==0){
            var style="margin-left:2em";
            htmlEl.setAttribute("style",style); 
    }
    RenderDocument.currentObjEl.appendChild(htmlEl);
    RenderDocument.currentObjEl=htmlEl;
    return view;
};

RenderDocument.RenderplainText=function(model,docObj){
   
    var view=new PlainTextView();
    view.build(docObj,model);
    var htmlEl=view.render();
    if(model.parent!==null &&  
        model.parent.parent!==null &&
        docObj.getObjLevelFromAncestor(RenderDocument.startingNode,model)!==0){
            var style="margin-left:2em";
            htmlEl.setAttribute("style",style); 
    }
    RenderDocument.currentObjEl.appendChild(htmlEl);
    RenderDocument.currentObjEl=htmlEl;
    return view;
};

RenderDocument.RenderInterpreterCommand=function(model,docObj){
    var view=new InterpreterCommandView();
    view.build(docObj,model);
    var htmlEl=view.render();
    if(model.parent!==null &&  
        model.parent.parent!==null &&
        docObj.getObjLevelFromAncestor(RenderDocument.startingNode,model)!==0){
            var style="margin-left:2em";
            htmlEl.setAttribute("style",style); 
    }
    RenderDocument.currentObjEl.appendChild(htmlEl);
    RenderDocument.currentObjEl=htmlEl;    
    return view;
};

RenderDocument.renderSubsystemLabel=function(interpreter){
    var labelEl=$("#subsystemLabel");
    var spec=interpreter.spec;
    var subsystemSpec=spec.getObjectByAddress("1");
    labelEl.html("CurrentSystem: "+subsystemSpec.getAttribute("subsystemDisplayName"));
};