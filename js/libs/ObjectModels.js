"use strict";
var ObjectModels={};


// a tree of objects
ObjectModels.ObjectModel=function(){
    this.parent=null;
    this.children=[];
    this.type="Object";
    this.docId=null; // each object has a unique id within the document it exists in.
    this.label=null;
    this.publicAttributes=["label"];
};

ObjectModels.ObjectModel.prototype.setAttribute=function(name,value){
    var attrObj;
    if(this.getAttributeObj(name)===null){
        attrObj=new ObjectModels.AttributeModel(value,name);
        attrObj.parent=this;
        this.children.push(attrObj);
    }else{
        attrObj=this.getAttributeObj(name);
        attrObj.value=value;
    }
    return attrObj;
};

ObjectModels.ObjectModel.prototype.getAttribute=function(name){
    var childObj;    
    for(var i=0;i<this.children.length;i++){
        childObj=this.children[i];
        if(childObj.type==="Attribute" && childObj.name===name){
            if(childObj.value!==null){
                return childObj.value;
            }
            return childObj;
        }
    }
    if(this.publicAttributes.indexOf(name)!==-1){
        return this[name];
    }
    return null;
};

ObjectModels.ObjectModel.prototype.getAttributeObj=function(name){
    var childObj;
    for(var i=0;i<this.children.length;i++){
        childObj=this.children[i];
        if(childObj.type==="Attribute" && childObj.name===name){            
            return childObj;
        }
    }
    return null;
};

// attributes can be stored both as childObjects and as javascript properties (this will be made more
// consistent in the future). This function does not return pointers to other objects, only primative attributes.
// Child attributes and javascript attributes can have the same name so javascript attributes are returned with a
// "jsVar_" in front of the name. This is primarily used for debugging.
ObjectModels.ObjectModel.prototype.getAllAttributes=function(){
    var attributes={},childObj;
    for(var i=0;i<this.children.length;i++){
        childObj=this.children[i];
        if(childObj.type==="Attribute" && childObj.children.length===0){
            attributes[childObj.name]=childObj.value;
        }
    }
    for(var key in this){
        if(this.hasOwnProperty(key) && ["parent","children"].indexOf(key)===-1){
            attributes["jsVar_"+key]=this[key];
        }
    }
    return attributes;
};

// get all javascript attributes only, not child object attributes
ObjectModels.ObjectModel.prototype.getJSAttributes=function(){
    var attributes={};
    for(var key in this){
        if(this.hasOwnProperty(key) && ["parent","children"].indexOf(key)===-1){
            attributes[key]=this[key];
        }
    }
    return attributes;
};

ObjectModels.ObjectModel.prototype.clone=function(){
    var jsonData=BaseSpecOperations.convertSpecObjToJSONString(this);
    var objCopy=BaseSpecOperations.convertJSONToSpecObj(JSON.parse(jsonData));
    return objCopy;
};

ObjectModels.ObjectModel.prototype.hasChildren=function(){
    if(this.children.length>0){
        return true;
    } else{
        return false;
    }
};

ObjectModels.ObjectModel.prototype.getOriginObj=function(){
    var obj=this;
    while(obj.parent!==null){
        obj=obj.parent;
    }
    return obj;
};

// An attribute is a primative value or an object with a name
ObjectModels.AttributeModel=function(value,name){
    ObjectModels.ObjectModel.call(this);
    this.value=value;
    if(name!==undefined){
        this.name=name;
    }
    else{
        this.name=null;
    }
    this.type="Attribute";
    this.publicAttributes=this.publicAttributes.concat(["value","name"]);
};

ObjectModels.AttributeModel.prototype=Object.create(ObjectModels.ObjectModel.prototype);

