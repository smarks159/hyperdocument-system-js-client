"use strict";
var Conditions={};

Conditions.required=function(value){
    if(value===null || value===undefined || value===""){
        return false;
    }
    else{
        return true;
    }
};


// search only in fileDB, used as condition to form input
Conditions.fileexists=function(value){
    var fileexists=false;
    $.ajax({
        url:"/webservice2/fileexists",
        type:"POST",
        data:{filename:"fileDB/"+value},
        success:function(data,textStatus,jqXHR){
            fileexists=data.fileexists;
        },
        async:false
    });
    return fileexists;
};

// search outside of fileDB (TODO: think of better names)
Conditions.fileexists2=function(value){
    var fileexists=false;
    $.ajax({
        url:"/webservice2/fileexists",
        type:"POST",
        data:{filename:value},
        success:function(data,textStatus,jqXHR){
            fileexists=data.fileexists;
        },
        async:false
    });
    return fileexists;
};

Conditions.interpreterspecExists=function(value){
  var filename="interpreterspecs/"+value;
  return Conditions.fileexists(filename);
};