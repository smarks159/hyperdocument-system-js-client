"use strict";

var GUIObjects={};

GUIObjects.InterpreterDisplay=function(interpreterEl){
    this.interpreterEl=interpreterEl;
};

GUIObjects.InterpreterDisplay.prototype.outputString=function(outString){
    var outputEl = document.createElement("span");
    outputEl.setAttribute("class", "interpreterOut");
    outputEl.innerHTML = outString;
    this.interpreterEl.append(outputEl);
};

GUIObjects.InterpreterDisplay.prototype.removeLastOutput=function(){
    var lastOutput = this.interpreterEl.children(".interpreterOut").last();
    lastOutput.remove();
};



GUIObjects.ActiveEvents=function(){
    this.activeEvents=[];
};

GUIObjects.ActiveEvents.prototype.activateEvent=function(eventObj){
    eventObj.activate();
    this.activeEvents.push(eventObj);
};

GUIObjects.ActiveEvents.prototype.deactivateAllEvents=function(){
    var i,eventObj;
    var length=this.activeEvents.length;
    for(i=0;i<length;i++){
        eventObj=this.activeEvents.pop();
        eventObj.deactivate();
    }
};

GUIObjects.ActiveEvents.prototype.deactivateKeyboardEvents=function(keynames){
    var i,j,eventObj,length;
    for(i=0;i<keynames.length;i++){       
        length=this.activeEvents.length;
        for(j=0;j<length;j++){
            eventObj=this.activeEvents[j];
            if(eventObj.keyname===keynames[i]){
                eventObj.deactivate();
            }
        };
    }
};

GUIObjects.keyboardEvent=function(keyname,action){
    this.keyname=keyname;
    this.action=action;
};

GUIObjects.keyboardEvent.prototype.activate=function(){
    keypress.register_combo({
            keys: this.keyname,
            prevent_repeat: true,
            prevent_default: true,
            on_keydown:this.action
    });
};

GUIObjects.keyboardEvent.prototype.deactivate=function(){
    keypress.unregister_combo(this.keyname);
};

GUIObjects.guiEvent=function(selector,event,action){
    this.selector=selector;
    this.event=event;
    this.action=action;
};

GUIObjects.guiEvent.prototype.activate=function(){
    $(this.selector).on(this.event,this.action);
};

GUIObjects.guiEvent.prototype.deactivate=function(){
    $(this.selector).off(this.event,this.action);
};


GUIObjects.inputForm=function(){    
    this.inputFormEl=null;
    this.buttonEl=null;
    this.hideForm=false;
    this.fields=[];
};

GUIObjects.inputForm.prototype.render=function(){
    var inputForm=$("<div id='inputForm' class='inputForm'></div>");  
    this.inputFormEl=inputForm;
    var fieldObj;
    for(var i=0;i<this.fields.length;i++){
        fieldObj=this.fields[i];
        inputForm.append(fieldObj.render());
        this.addSpace();
        if(fieldObj.isJavascriptObj){
            fieldObj.attachJavascript();
        }
    }
    var button=$("<button>submit</button>");
    button.attr("id","inputSubmit");
    this.buttonEl=button;
    inputForm.append(button);
    if(this.hideForm){
        this.hide();
    }
    return inputForm;
};

GUIObjects.inputForm.prototype.getButtonEl=function(){
    return this.buttonEl;
};

GUIObjects.inputForm.prototype.remove=function(){
    this.inputFormEl.remove();
    this.inputFormEl=null;
    this.buttonEl=null;
};

GUIObjects.inputForm.prototype.isVisible=function(){
    if(this.inputFormEl===null){
        return false;
    }
    else{
        return this.inputFormEl.is(":visible");
    }
};

GUIObjects.inputForm.prototype.addField=function(fieldObj){
    this.fields.push(fieldObj);
};

GUIObjects.inputForm.prototype.getData=function(){
    var i,fieldObj;
    var data={};
    for(i=0;i<this.fields.length;i++){
        fieldObj=this.fields[i];        
        if(fieldObj.hasData){
            if(fieldObj.fieldName===null || fieldObj.fieldName===undefined){
                throw new Error("Trying to get data of a field without a fieldName");
            }
            data[fieldObj.fieldName]=fieldObj.getData();
        }
    }
    return data;
};

GUIObjects.inputForm.prototype.addSpace=function(){
    this.inputFormEl.append($("<div></div>"));
};

GUIObjects.inputForm.prototype.show=function(){
    this.inputFormEl.css('display','block');
};

GUIObjects.inputForm.prototype.hide=function(){
    this.inputFormEl.css('display','none');
};

GUIObjects.inputForm.prototype.focusOnFirstInputField=function(){
    var i;
    for(i=0;i<this.fields.length;i++){
        if(this.fields[i].hasData){
            this.fields[i].focus();
            break;
        }
    }
};

GUIObjects.outputField=function(text){
    this.fieldEl=null;
    this.hasData=false;
    this.isJavascriptObj=false;
    this.validator=null;
    this.text=text;
};

GUIObjects.outputField.prototype.render=function(){
    var field=$("<span>"+this.text+"</span>");
    this.fieldEl=field;
    return field;
};

GUIObjects.stringLineField=function(fieldName){
    this.fieldEl=null;
    this.hasData=true;
    this.isJavascriptObj=false;
    this.validator=null;
    this.defaultValue="";
    this.fieldName=fieldName;
};

GUIObjects.stringLineField.prototype.render=function(){
    var inputField=$("<input></input>");
    inputField.attr("name",this.fieldName);
    inputField.attr("autocomplete","off");
    inputField.css("width","100%");
    this.fieldEl=inputField;
    this.setData(this.defaultValue);
    return this.fieldEl;
};

GUIObjects.stringLineField.prototype.getData=function(){
    return this.fieldEl.val();
};

GUIObjects.stringLineField.prototype.setData=function(text){
    this.fieldEl.val(text);
};

GUIObjects.stringLineField.prototype.focus=function(){
    this.fieldEl.focus();
};

GUIObjects.autocompleteField=function(fieldName){
    this.fieldEl=null;
    this.hasData=true;
    this.isJavascriptObj=true;
    this.validator=null;
    this.defaultValue="";
    this.dataSource=null;
    this.fieldName=fieldName;
};

GUIObjects.autocompleteField.prototype.render=function(){
    var inputField=$("<input></input>");
    inputField.attr("name",this.fieldName);
    inputField.attr("autocomplete","off");
    inputField.css("width","100%");
    this.fieldEl=inputField;
    this.setData(this.defaultValue);
    return inputField;
};


GUIObjects.autocompleteField.prototype.attachJavascript=function(){
    var that=this;
    this.fieldEl.autocomplete(
            {
                source:function(request,response){
                        var regex=new RegExp("^"+$.ui.autocomplete.escapeRegex(request.term));
                        var results=[],i,dataObj;
                        for(i=0;i<that.dataSource.length;i++){
                            dataObj=that.dataSource[i];
                            if(regex.test(dataObj.value)){
                                results.push(dataObj);
                            }
                        }
                        response(results);
                    }
            }
    );
};

GUIObjects.autocompleteField.prototype.getData=function(){
    return this.fieldEl.val();
};

GUIObjects.autocompleteField.prototype.setData=function(text){
    this.fieldEl.val(text);
};

GUIObjects.autocompleteField.prototype.focus=function(){
    this.fieldEl.focus();
};


GUIObjects.fileAutocompleteField=function(fieldName,url){
    GUIObjects.autocompleteField.call(this,fieldName);
};
GUIObjects.fileAutocompleteField.prototype=Object.create(GUIObjects.autocompleteField.prototype);
GUIObjects.fileAutocompleteField.prototype.attachJavascript=function(){
    var that=this;
    this.fieldEl.autocomplete(
            {
                delay:10,
                source:function(request,response){
                        $.ajax({
                            url:"/webservice2/fileAutoComplete",
                            type:"POST",
                            data:{searchString:request.term},
                            success:function(data,textStatus,jqXHR){
                                var i,fileName;
                                var results=[];
                                if(data.filesFound){
                                    for(i=0;i<data.filesFound.length;i++){
                                        var filePathArray=data.filesFound[i].split("/");
                                        var fileName=filePathArray[filePathArray.length-1];
                                        if(data.isDirectory[i]===false){
                                            results.push({label:fileName+" ['F']",value:data.filesFound[i]});
                                        } else{
                                            results.push({label:fileName+" ['D']",value:data.filesFound[i]});
                                        }
                                    }
                                    response(results);
                                }
                            }
                            
                        });
                }
            }
    );
};


GUIObjects.plainTextEditorField=function(fieldName){
    this.fieldEl=null;
    this.hasData=true;
    this.isJavascriptObj=true;
    this.validator=null;
    this.defaultValue="";
    this.fieldName=fieldName;
};

GUIObjects.plainTextEditorField.prototype.render=function(){
    var inputField=$("<textarea></textarea>");
    inputField.attr("name",this.fieldName);
    inputField.attr("class","textInputField");
    this.fieldEl=inputField;
    this.setData(this.defaultValue);
    return this.fieldEl;
};

GUIObjects.plainTextEditorField.prototype.getData=function(){
    return this.fieldEl.val();
};

GUIObjects.plainTextEditorField.prototype.setData=function(text){
    this.fieldEl.val(text);
};

GUIObjects.plainTextEditorField.prototype.focus=function(){
    this.fieldEl.focus();
    this.fieldEl.height('auto');
    this.fieldEl.height($(this.fieldEl)[0].scrollHeight);
};

GUIObjects.plainTextEditorField.prototype.attachJavascript=function(){
    $(this.fieldEl).on('keydown',function(){
        $(this).height('auto');
        $(this).height(this.scrollHeight);
    });
};

