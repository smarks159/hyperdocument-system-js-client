"use strict";
var InMemoryHistory={};

InMemoryHistory.NavigationHistory=function(maxHistory){
    this.backward=[];
    this.foward=[];
    this.current=null;
    if(maxHistory===undefined){
        this.maxHistory=50;
    } else{
        this.maxHistory=maxHistory;
    }
};

// this function needs to be called after jumping or displaying a new page.
InMemoryHistory.NavigationHistory.prototype.setCurrent=function(object){
    if(this.current!==null){
        if(this.backward.length===this.maxHistory){
            this.backward.shift();
        }  
        this.backward.push(this.current);
    }
    this.current=object;
    this.foward=[];
};

InMemoryHistory.NavigationHistory.prototype.goBack=function(){
    if(this.backward.length===0){
        return null;
    }
    this.foward.push(this.current);
    this.current=this.backward.pop();
    return this.current;
};

InMemoryHistory.NavigationHistory.prototype.goFoward=function(){
    if(this.foward.length===0){
        return null;
    }
    this.backward.push(this.current);
    this.current=this.foward.pop();
    return this.current;
};

InMemoryHistory.DesktopNavigationHistory=function(){
    this.center=new InMemoryHistory.NavigationHistory();
    this.left=new InMemoryHistory.NavigationHistory();
    this.right=new InMemoryHistory.NavigationHistory();
    this.up=new InMemoryHistory.NavigationHistory();
    this.down=new InMemoryHistory.NavigationHistory();
};

InMemoryHistory.jumpHistoryObj=function(selectedRootObj,fileName){
    this.selectedRootObj=selectedRootObj;
    this.fileName=fileName;
};

InMemoryHistory.jumpHistoryObj.prototype.clone=function(){
    return new InMemoryHistory.jumpHistoryObj(this.selectedRootObj.clone());
};






