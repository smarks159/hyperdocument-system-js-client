"use strict";
var Interpreter={};

Interpreter.Interpreter=function(interpreterSpec,interpreterEl){
    this.spec=interpreterSpec;
    this.interpreterEl=interpreterEl;
    this.display=new GUIObjects.InterpreterDisplay(interpreterEl);
    this.activeEvents=new GUIObjects.ActiveEvents();
    this.setInitialState();            
    this.process();
    var that=this;
    var cancelEvent=new GUIObjects.keyboardEvent("esc",function(event){
        that.ResetInterpreter();
    });
    cancelEvent.activate();
};

Interpreter.Interpreter.prototype.setInitialState=function(){
    this.subsystemNamespace=null;
    this.inputData={};
    this.resetCalled=false;
};

Interpreter.Interpreter.prototype.process=function(){    
    var currentObj=this.spec.selectedRootObj;
    if(currentObj.parent===null){
        BaseSpecOperations.jumpToElement(this.spec,"1");
        currentObj=this.spec.selectedRootObj;
    }
    var command=currentObj.getAttribute("commandName");
    if(this[command]!==undefined){
        this[command]();
    }
    else{
        throw new Error("The command " + command + " is not supported by the interpreter");
    }
};

Interpreter.Interpreter.prototype.InitializeSubsystem=function(){
    var currentObj=this.spec.selectedRootObj;
    this.subsystemNamespace=currentObj.getAttribute("subsystemNamespace");
    BaseSpecOperations.jumpToSuccessor(this.spec);
    this.process();
};

Interpreter.Interpreter.prototype.SelectCommand=function(){
    this.display.outputString("(Enter Command)");
    var commands=BaseSpecOperations.findChildrenByAttributes(this.spec,{"type":"InterpreterCommand","commandName":"Command"});
    var commandObj,letters=[],i;       
    for(i=0;i<commands.length;i++){
        commandObj=commands[i];
        if(commandObj.getAttribute("letter").length===1){
            letters.push(commandObj.getAttribute("letter"));        
        }
    }
    this.activeEvents.deactivateAllEvents();
    var letter;
    var keyEvent;
    for(var i=0;i<letters.length;i++){
        letter=letters[i];
        if(letter.length===1){
            var createCallback=function(letter){
                return function(event){
                    $("#inputForm").find("input[name=commandSelected]").val(letter); 
                    $("#inputForm").find("button").trigger('click');
                };
            };
            keyEvent=new GUIObjects.keyboardEvent(letter,createCallback(letter));
            this.activeEvents.activateEvent(keyEvent);
        }
    };
    
    var fieldDataSource=[];
    for(i=0;i<commands.length;i++){
        commandObj=commands[i];
        fieldDataSource.push({label:commandObj.getAttribute("letter")+"("+commandObj.getAttribute("name")+")",value:commandObj.getAttribute("letter")})
    }
    
    var inputForm=new GUIObjects.inputForm();
    inputForm.addField(new GUIObjects.outputField("Enter Command"));
    var autoField=new GUIObjects.autocompleteField("commandSelected");
    autoField.dataSource=fieldDataSource;
    inputForm.addField(autoField);  
    inputForm.render();
    inputForm.hide();
    this.display.interpreterEl.after(inputForm.inputFormEl);
    
    var button=inputForm.getButtonEl();
    var that=this;    
    button.click(function(){
        var commandSelected=inputForm.getData().commandSelected; 
        inputForm.remove();
        var command = BaseSpecOperations.findChildrenByAttributes(that.spec,{"letter": commandSelected})[0];
        that.display.removeLastOutput();
        that.display.outputString(command.getAttribute("name"));
        var next=command.getAttribute("next");
        that.activeEvents.deactivateAllEvents();
        BaseSpecOperations.jumpToElement(that.spec,next);     
        BaseSpecOperations.jumpDown(that.spec);
        that.process();
    });
    
    this.activeEvents.activateEvent(new GUIObjects.keyboardEvent("space",function(event){
        inputForm.show();
        that.activeEvents.deactivateKeyboardEvents(letters);
        inputForm.focusOnFirstInputField();   
    }));
};


Interpreter.Interpreter.prototype.CommandInput=function(){
    this.display.outputString("(Enter Command)");
    var currentObj=this.spec.selectedRootObj;
    var commands=currentObj.getAttribute("commands");
    var letters=commands.split(",");
    this.activeEvents.deactivateAllEvents();
    var letter;
    var keyEvent;
    for(var i=0;i<letters.length;i++){
        letter=letters[i];
        if(letter.length===1){
            var createCallback=function(letter){
                return function(event){
                    $("#inputForm").find("input[name=commandSelected]").val(letter); 
                    $("#inputForm").find("button").trigger('click');
                };
            };
            keyEvent=new GUIObjects.keyboardEvent(letter,createCallback(letter));
            this.activeEvents.activateEvent(keyEvent);
        }
    };
    
    var fieldDataSource=[];
    var values=currentObj.getAttribute("values").split(",");
    for(i=0;i<letters.length;i++){
        fieldDataSource.push({label:letters[i]+"("+values[i]+")",value:letters[i]});
    }
    
    var inputForm=new GUIObjects.inputForm();
    inputForm.addField(new GUIObjects.outputField("Enter Command"));
    var autoField=new GUIObjects.autocompleteField("commandSelected");
    autoField.dataSource=fieldDataSource;
    inputForm.addField(autoField);  
    inputForm.render();
    inputForm.hide();
    this.display.interpreterEl.after(inputForm.inputFormEl);
    
    var button=inputForm.getButtonEl();
    var that=this;
    button.click(function(){
        var commandSelected=inputForm.getData().commandSelected; 
        inputForm.remove();
        var i=0;
        for(i;i<letters.length;i++){
            if(letters[i]===commandSelected){
                break;
            }
        }
        var value = values[i];
        that.display.removeLastOutput();
        that.display.outputString(value); 
        that.activeEvents.deactivateAllEvents();
        that.inputData[currentObj.getAttribute("valueVar")]=value;
        BaseSpecOperations.jumpToSuccessor(that.spec); 
        that.process();
    });
    
    this.activeEvents.activateEvent(new GUIObjects.keyboardEvent("space",function(event){
        inputForm.show();
        that.activeEvents.deactivateKeyboardEvents(letters);
        inputForm.focusOnFirstInputField();  
    }));
};

Interpreter.Interpreter.prototype.Output=function(){
    var outObj=this.spec.selectedRootObj;
    this.display.outputString(outObj.getAttribute("output"));
    BaseSpecOperations.jumpToSuccessor(this.spec);
    this.process();
};

Interpreter.Interpreter.prototype.InputForm=function(){
    var formSpec=this.spec.selectedRootObj;
    $("#inputForm").remove();
    var inputForm = new GUIObjects.inputForm();    
    BaseSpecOperations.jumpToElement(this.spec,formSpec.getAttribute("fields"));
    var fieldObj=BaseSpecOperations.jumpDown(this.spec);
    
    var fieldType,inputField;    
    var inputFields=[];    
    while(fieldObj!==null){        
        fieldType=fieldObj.getAttribute("fieldType");
        if(Interpreter.Fields[fieldType]===undefined){
            throw new Error("The field type '"+fieldType+"' is not currently supported")
        }
        inputField=Interpreter.Fields[fieldType](fieldObj,inputForm,this);
        inputForm.addField(inputField);
        if(fieldType!=="textout"){
            inputFields.push(inputField);
        }
        fieldObj=BaseSpecOperations.jumpToSuccessorNoError(this.spec);
    }
    inputForm.render();
    this.interpreterEl.after(inputForm.inputFormEl);
    var button=inputForm.getButtonEl();
    var that=this;
    BaseSpecOperations.jumpToElement(this.spec,formSpec);
    button.click(function(){
        button.trigger("beforeSubmit");
        $.each(inputForm.getData(),function(key,value){
            that.inputData[key]=value;
        });
        
        var errors={};
        BaseSpecOperations.jumpToElement(that.spec,formSpec.getAttribute("fields"));
        var fields=BaseSpecOperations.findChildrenByAttributes(that.spec,{"type":"fieldObj"});
        var i,j,field,conditions,condition,name;
        for(i=0;i<fields.length;i++){
            field=fields[i];            
            if(field.getAttribute("conditions")===null){
                continue;
            }
            errors[field.getAttribute("varName")]=[];
            BaseSpecOperations.jumpToElement(that.spec,field.getAttribute("conditions"));
            conditions=BaseSpecOperations.findChildrenByAttributes(that.spec,{"type":"condition"});
            for(j=0;j<conditions.length;j++){
                condition=conditions[j];
                name=condition.getAttribute("name");
                if(Conditions[name](that.inputData[field.getAttribute("varName")])===false){
                    errors[field.getAttribute("varName")].push(condition.getAttribute("message"));
                }
            }
        }
        var noErrors=true;
        var fieldErrs,errorEl;
        $.each(errors,function(key,value){
            fieldErrs=errors[key];
            $("#inputForm").find(".inputError").remove();
            if(fieldErrs.length!==0){
                noErrors=false;                
                errorEl=$("<div class='inputError'></div>");
                errorEl.css("color","red");
                errorEl.html(fieldErrs.join(";"));
                $("#inputForm").find("input[name="+key+"]").after(errorEl);
            }
        });
        
        if(noErrors){
            inputForm.remove();
            BaseSpecOperations.jumpToElement(that.spec,formSpec);
            BaseSpecOperations.jumpToSuccessor(that.spec);
            that.activeEvents.deactivateAllEvents();
            that.process();
        }
    });    
    inputForm.focusOnFirstInputField();
};

Interpreter.Interpreter.prototype.ExecuteFunction=function(){
    var actionObj=this.spec.selectedRootObj;
    var functionName= actionObj.getAttribute("functionName");
    var parameterNames;
    if(actionObj.getAttribute("parameters")!==null){
        parameterNames=actionObj.getAttribute("parameters").split(",");        
    }
    else{
        parameterNames=[];
    }
    
    // TODO use messages to communicate with the output document rather than hardcoding the dependencies here
    var parameterVals,functionNamespace;
    try{
        if(actionObj.getAttribute("namespace")===null){
            parameterVals=[GuiEnv.docObjs[GuiEnv.docSelected]];
            functionNamespace = window[this.subsystemNamespace];
        }
        else if(actionObj.getAttribute("namespace")==="interpreter"){
            parameterVals=[];
            functionNamespace=this;
        }
        else{
            parameterVals=[GuiEnv.docObjs[GuiEnv.docSelected]];
            functionNamespace=window[actionObj.getAttribute("namespace")];
        }
        for (var i = 0; i < parameterNames.length; i++) {
            parameterVals.push(this.inputData[parameterNames[i]]);
        }
        if(functionNamespace[functionName]===undefined){
            throw new Error("The function name " + functionName + " is not defined");
        }
        if(actionObj.getAttribute("namespace")==="interpreter"){
            functionNamespace[functionName].apply(this, parameterVals);
        }
        else{
            functionNamespace[functionName].apply(null, parameterVals);
        }
        if(actionObj.getAttribute("renderDocument")!==false){
            // TODO: render only the documents that have been changed.
            $.each(GuiEnv.docObjs,function(key,value){
                var docEl = $("#doc"+key);                
                if(docEl.is(":visible")){                    
                    // preserve the scroll position when the document is rerendered
                    var docContainer = docEl.closest(".documentOut");
                    var scrollTop = docContainer.scrollTop();
                    var scrollLeft = docContainer.scrollLeft();
                    
                    RenderDocument.renderDocument(GuiEnv.docObjs[key],undefined,key);
                    for(var i=0;i<RenderDocument.initFuncs.length;i++){
                        RenderDocument.initFuncs[i].attachJavascript();
                    }
                    RenderDocument.initFuncs=[];
                    // must reselect the docContainer because the existing in the dom is destroyed
                    docContainer = $("#doc"+key).closest(".documentOut");
                    docContainer.scrollTop(scrollTop);
                    docContainer.scrollLeft(scrollLeft);
                }
            });
        }
        if(this.resetCalled===false){
            BaseSpecOperations.jumpToSuccessor(this.spec);        
            this.process();
        }else{
            this.resetCalled=false;
        }
    }
    catch(e){
        if(e.name==="SpecSystemError"){
            this.showErrorMessage(e.message);
        }
        throw e;
    }
    
};

Interpreter.Interpreter.prototype.ResetInterpreter=function(){
    BaseSpecOperations.jumpToElement(this.spec,"0");
    this.interpreterEl.children().remove();    
    $("#inputForm").remove();
    
    this.activeEvents.deactivateAllEvents();
    
    // remove hightlighting of selected objects
    $(".selected").css("background-color", "");
    $(".selected").removeClass("selected");
    
    this.setInitialState();
    this.clearErrorMessage();
    this.process();
};

Interpreter.Interpreter.prototype.showErrorMessage=function(errorMsg){
  $("#interpreterError").css("display","block");
  $("#interpreterError").html(errorMsg);
};

Interpreter.Interpreter.prototype.clearErrorMessage=function(){
  $("#interpreterError").css("display","none");
  $("#interpreterError").html("");
};


// get the attributes from a selected object and set it to variables in the interpter memory.
Interpreter.Interpreter.prototype.getObjectAttributes=function(obj){
    var funcSpec=this.spec.selectedRootObj;
    var attrNames=funcSpec.getAttribute("attrsToGet").split(",");
    var valuesToSet=funcSpec.getAttribute("valuesToSet").split(",");
    if(attrNames.length!==valuesToSet.length){
        throw SpecSystemError("The number of attributes and values to set must be equal");
    }
    obj=BaseSpecOperations.getObjectFromAddress(GuiEnv.docObjs[GuiEnv.docSelected],obj);
    for(var i=0;i<attrNames.length;i++){
        this.inputData[valuesToSet[i]]=obj.getAttribute(attrNames[i]);
    }
};

Interpreter.Interpreter.prototype.scrollToElement=function(dataObj){
    if(dataObj.viewObj!==null){
        var objEl=dataObj.viewObj.htmlEl;
        var docEl=$(objEl).closest(".documentOut");
        docEl.scrollTop(objEl.position().top-docEl.position().top + docEl.scrollTop());
    }
};

// Actions to collection user Input (needs refactoring)
Interpreter.Fields={};

Interpreter.Fields.textout=function(fieldObj,inputForm,interpreter){
    var text=fieldObj.getAttribute("string");
    if(text===null){
        text="text is null";
    }
    return new GUIObjects.outputField(text);
};

Interpreter.Fields.getAddress=function(fieldObj,inputForm,interpreter){
    var inputField=new GUIObjects.stringLineField(fieldObj.getAttribute("varName"));    
    inputForm.hideForm=true;
    interpreter.display.outputString("[Enter Address]");
    
    var that=interpreter;
    var getAddressListener=function(event){
        // check to see if the user clicked on the top level document object
        var selectedObject;
        var selectedWindow=$(event.target).closest("[data-windowId]").attr("data-windowId");
        if ($(".documentOut").is(event.target)) {
            inputField.setData("0"+","+selectedWindow);
        }
        else{
            selectedObject=$(event.target).closest("[data-structuralpath]");
            var path=selectedObject.attr("data-structuralpath");            
            // In the center there is a growable blank div to keep the * in the correct place,
            // this causes the user to click on the growable div when the center window is empty
            // instead of the root document object.
            if(selectedObject.length===0 && selectedWindow==="center"){
                path="0";
            }
            inputField.setData(path+","+selectedWindow);
        }        
        var button=inputForm.getButtonEl();
        button.one("beforeSubmit",{outputAddress:false},highlightAddress);
        button.trigger('click');        
    };
    
    // highlight the address whether it is specified by clicking on an object or typing in an address.
    var highlightAddress=function(event){
        var path=inputField.getData().split(",");
        var windowPath;
        var objectPath;
        objectPath=path[0].trim();
        if(path.length===1){
            windowPath=GuiEnv.docSelected;            
        }
        else{
            windowPath=path[1].trim();
        }
        interpreter.display.removeLastOutput();
        // highlight the selected object, if it is not the origin
        var selectedWindow=$("[data-windowId='"+windowPath+"']");
        var selectedObject=selectedWindow.find("[data-structuralpath='"+objectPath.toUpperCase()+"']");        
        if(objectPath!=="0"){
            var highlightColor;
            if(fieldObj.getAttribute("highlightColor")!==null){
                highlightColor=fieldObj.getAttribute("highlightColor");
            }
            else{
                highlightColor="yellow";
            }
            selectedObject.css("background-color",highlightColor);
            selectedObject.addClass("selected");
            if(event.data.outputAddress){
                that.display.outputString("(address) "+objectPath+","+windowPath);
            }
            that.display.outputString("[highlighted in " + highlightColor+"]");
        }
        else{
            that.display.outputString("[rootselected in window "+windowPath+"]");
        }        
    };
    interpreter.activeEvents.activateEvent(new GUIObjects.guiEvent(".documentOut","click",getAddressListener));    
    
    interpreter.activeEvents.activateEvent(new GUIObjects.keyboardEvent("space",function(event){
        inputForm.show();
        inputForm.focusOnFirstInputField();
        inputForm.getButtonEl().one("beforeSubmit",{outputAddress:true},highlightAddress);
    }));
    return inputField;
};

Interpreter.Fields.getWindow=function(fieldObj,inputForm,interpreter){
    var inputField=new GUIObjects.stringLineField(fieldObj.getAttribute("varName"));    
    inputForm.hideForm=true;    
    interpreter.display.outputString("[Select Window]");
    var getWindowListener=function(event){
        interpreter.display.removeLastOutput();
        var selectedWindow=$(event.target).closest("[data-windowId]").attr("data-windowId");
        interpreter.display.outputString("["+selectedWindow+" window"+"]");
        inputField.setData(selectedWindow);
        inputForm.getButtonEl().trigger('click');
    };
    interpreter.activeEvents.activateEvent(new GUIObjects.guiEvent(".documentOut","click",getWindowListener));
    interpreter.activeEvents.activateEvent(new GUIObjects.keyboardEvent("space",function(event){
        inputForm.show();
        inputForm.focusOnFirstInputField();  
    }));
    return inputField;
};

Interpreter.Fields.getWord=function(fieldObj,inputForm,interpreter){
    var inputField=new GUIObjects.stringLineField(fieldObj.getAttribute("varName"));
    inputForm.hideForm=true;
    if(fieldObj.getAttribute("prompt")!==null){
        interpreter.display.outputString("["+fieldObj.getAttribute("prompt")+"]");
    } else{
        interpreter.display.outputString("[select word]");
    }
    
    var that=interpreter;
    var getWordListener=function(event){
        var selectedElement=$(event.target);
        
        // if selected element is not a link display an error message (todo, seperate out error display into seperate object)
        var elementType=fieldObj.getAttribute("elementType");
        if(elementType===null){
            elementType="word";
        }
        if(!selectedElement.hasClass(elementType) && elementType!=="any"){
            
        } else{
            //TODO: remove the error message if it is being displayed
            
            var highlightColor;
            if(fieldObj.getAttribute("highlightColor")!==null){
                highlightColor=fieldObj.getAttribute("highlightColor");
            }
            else{
                highlightColor="yellow";
            }
            selectedElement.css("background-color",highlightColor);
            selectedElement.addClass("selected");
            that.display.outputString("[highlighted in " + highlightColor+"]");
            interpreter.display.removeLastOutput();
            var selectedWindow=$(event.target).closest("[data-windowId]").attr("data-windowId");
            if(elementType==="hyperdoclink"){
               inputField.setData(selectedElement.attr("data-link")+";"+selectedWindow);
            } else{
                inputField.setData(selectedElement.text()+";"+selectedWindow);
            }
            inputForm.getButtonEl().trigger('click');
        }
    };
    
    interpreter.activeEvents.activateEvent(new GUIObjects.guiEvent(".documentOut","click",getWordListener));
    
    return inputField;
    
};

Interpreter.Fields.textEntry=function(fieldObj,inputForm,interpreter){
    var inputField=new GUIObjects.stringLineField(fieldObj.getAttribute("varName"));
    if(fieldObj.getAttribute("defaultValue")!==null){
        var value;
        if(typeof fieldObj.getAttribute("defaultValue")==="string"){
            // a default form value can be defined either as a parameter name or an actual value.   
            if (interpreter.inputData[fieldObj.getAttribute("defaultValue")] !== undefined) {
                value = interpreter.inputData[fieldObj.getAttribute("defaultValue")];
            }
            else {
                value = fieldObj.getAttribute("defaultValue");
            }
        }
        else{
            var parameterObj=fieldObj.getAttribute("defaultValue");
            var location=parameterObj.getAttribute("location");
            if(location==="document"){
                value=GuiEnv.docObjs[GuiEnv.docSelected][parameterObj.getAttribute("name")];
                if(value===undefined){
                    throw SpecSystemError("The attribute "+parameterObj.getAttribute("name")+" is not defined in the document");
                }
            }
        }
        if(value instanceof String){
            value=value.trim();
        }
        inputField.defaultValue=value;    
    }
    return inputField;
};

Interpreter.Fields.plainTextEditor=function(fieldObj,inputForm,interpreter){
    var editorField=new GUIObjects.plainTextEditorField(fieldObj.getAttribute("varName"));    
    
    if(fieldObj.getAttribute("defaultValue")!==null){
        var value;
        if(typeof fieldObj.getAttribute("defaultValue")==="string"){
            // a default form value can be defined either as a parameter name or an actual value.   
            if (interpreter.inputData[fieldObj.getAttribute("defaultValue")] !== undefined) {
                value = interpreter.inputData[fieldObj.getAttribute("defaultValue")];
            }
            else {
                value = fieldObj.getAttribute("defaultValue");
            }
        }
        else{
            var parameterObj=fieldObj.getAttribute("defaultValue");
            var location=parameterObj.getAttribute("location");
            if(location==="document"){
                value=GuiEnv.docObjs[GuiEnv.docSelected][parameterObj.getAttribute("name")];
                if(value===undefined){
                    throw SpecSystemError("The attribute "+parameterObj.getAttribute("name")+" is not defined in the document");
                }
            }
        }
        if(value instanceof String){
            value=value.trim();
        }
        editorField.defaultValue=value;
    }
    return editorField;
};


Interpreter.Fields.fileAutocomplete=function(fieldObj,inputForm,interpreter){
    var inputField=new GUIObjects.fileAutocompleteField(fieldObj.getAttribute("varName"));
    if(fieldObj.getAttribute("defaultValue")!==null){
        var value;
        if(typeof fieldObj.getAttribute("defaultValue")==="string"){
            // a default form value can be defined either as a parameter name or an actual value.   
            if (interpreter.inputData[fieldObj.getAttribute("defaultValue")] !== undefined) {
                value = interpreter.inputData[fieldObj.getAttribute("defaultValue")];
            }
            else {
                value = fieldObj.getAttribute("defaultValue");
            }
        }
        else{
            var parameterObj=fieldObj.getAttribute("defaultValue");
            var location=parameterObj.getAttribute("location");
            if(location==="document"){
                value=GuiEnv.docObjs[GuiEnv.docSelected][parameterObj.getAttribute("name")];
                if(value===undefined){
                    throw SpecSystemError("The attribute "+parameterObj.getAttribute("name")+" is not defined in the document");
                }
            }
        }
        if(value instanceof String){
            value=value.trim();
        }
        inputField.defaultValue=value;    
    }
    return inputField;
};