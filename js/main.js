"use strict";
var GuiEnv={
    docSelected:"center",
    docObjs:{
        center:new DocumentModels.DocumentViewModel(),
        left:new DocumentModels.DocumentViewModel(),
        right:new DocumentModels.DocumentViewModel(),
        up:new DocumentModels.DocumentViewModel(),
        down:new DocumentModels.DocumentViewModel()
    },
    jumpHistory:new InMemoryHistory.DesktopNavigationHistory()
};

// remember which documents are loaded and the layout state
window.onbeforeunload=function(){
    var documentState={};    
    $.each(GuiEnv.docObjs,function(key,value){
        if(this.filename!==""){
            documentState[key+"_filename"]=value.filename;
            documentState[key+"_levelsDownLimit"]=value.levelsDownLimit;
            // must preserve the data in the document because it may not be saved to file yet.
            documentState["data_"+this.filename]=BaseSpecOperations.convertDocObjToJSONString(value);
        }
        else{
            documentState[key]=BaseSpecOperations.convertDocObjToJSONString(value);
        }
        documentState[key+"_rootObjectInView"]=value.getObjStructuralPath(value.selectedRootObj);
    });
    documentState["layoutState"]={};
    $.each(["left","right","center","down"],function(index,value){
        var windowPane=$("[data-windowId="+value+"]");
        documentState["layoutState"][value]={"flex-basis":windowPane.css("flex-basis"),"display":windowPane.css("display")};
    });
    var documentStateString=JSON.stringify(documentState);
    $.ajax({
        url:"/webservice2/saveJSONFile",
        type:"POST",
        data:{filename:"state/documentState",data:documentStateString},
        async:false
    });
};

// load documents from saved state on app initialize
var documentState;
var error=false,errorMsg;
if(Conditions.fileexists2("state/documentState")){
    $.ajax({
           url:"/webservice2/loadJSON",
           type:"POST",
           data:{fileName:"state/documentState.json"},
           success:function(data,textStatus,jqXHR){
               documentState=data;
               var filesLoaded={};
               $.each(GuiEnv.docObjs,function(key,value){
                   if(documentState[key+"_filename"]!==undefined){
                       var fileName=documentState[key+"_filename"];
                       if(filesLoaded[fileName]===undefined){
                            GuiEnv.docObjs[key]=BaseSpecOperations.convertJSONToSpecDoc(JSON.parse(documentState["data_"+fileName]));
                            GuiEnv.docObjs[key].levelsDownLimit=data[key+"_levelsDownLimit"];
                            filesLoaded[fileName]=key;
                        } else{
                            // the same file loaded in multiple windows must be synchronized
                            BaseSpecOperations.copyDocument(null,filesLoaded[fileName],key);
                        }
                        
                   } else{
                       GuiEnv.docObjs[key]=BaseSpecOperations.convertJSONToSpecDoc(JSON.parse(documentState[key]));              
                   }
                   BaseSpecOperations.jumpToElement(GuiEnv.docObjs[key],documentState[key+"_rootObjectInView"]);
                   BaseSpecOperations.addToJumpHistory(GuiEnv.docObjs[key],key);
               });
               $.each(["left","right","center","down"],function(index,value){
                    var windowPane=$("[data-windowId="+value+"]");
                    windowPane.css("flex-basis",documentState["layoutState"][value]["flex-basis"]);
                    windowPane.css("display",documentState["layoutState"][value]["display"]);
                });
           },
           error:function(data,textStatus,jqXHR){   
               jsonData=data;
               error=true;
               errorMsg=jsonData.error.message;
           },
           async:false
   });
   if(error){
           throw SpecSystemError(errorMsg);
   }
}

var interpreterSpec=BaseSpecOperations.loadFromJSON("/interpreterspecs/BaseSpecOperations");
var interpreter=new Interpreter.Interpreter(interpreterSpec,$("#commandInterpreter"));

RenderDocument.renderSubsystemLabel(interpreter);

$.each(GuiEnv.docObjs,function(key,value){
        var docEl = $("#doc"+key);                
        if(docEl.is(":visible")){             
            docEl = RenderDocument.renderDocument(GuiEnv.docObjs[key],undefined,key);
            for(var i=0;i<RenderDocument.initFuncs.length;i++){
                RenderDocument.initFuncs[i].attachJavascript();
            }
            RenderDocument.initFuncs=[];
        }
});


$(".pane").mousedown(function(){
    var windowId=$(this).attr("data-windowId");
    GuiEnv.docSelected=windowId;
    $(".selectedWindow").removeClass("selectedWindow");
    $(this).addClass("selectedWindow");
    // must resize all windows, because selectedWindow changes the border size
    //desktop.resizeAll();
});

$(window).resize(function(){
    $("#viewport").height($(window).height()-10);
    $("#viewport").width($(window).width()-10);
});

// For flexbox to work properly the height and width of the parent container must explicitly be set
window.onload=function(){
    $("#viewport").height($(window).height()-10);
    $("#viewport").width($(window).width()-10);
};

$("[data-windowId='left']").resizable({
        handles:"e"
}).on("resize",function( event, ui){ 
    // convert px size to percentage to maintain proper window size when resizing the desktop
    var percentNum =(ui.size.width/$("#desktop").width()) * 100;
    $(this).css("flex-basis",percentNum+"%");

});

$("[data-windowId='right']").resizable({
        handles:"w"
}).on("resize",function( event, ui){ 
    // convert px size to percentage to maintain proper window size when resizing the desktop
    var percentNum =(ui.size.width/$("#desktop").width()) * 100;
    $(this).css("flex-basis",percentNum+"%");
    // jquery resize changes left position as well as width, must reset to default as we do not want top to change with flexbox
    $(this).css("left","auto");

});

$("[data-windowId='up']").resizable({
        handles:"s"
}).on("resize",function( event, ui){ 
    // convert px size to percentage to maintain proper window size when resizing the desktop
    var percentNum =(ui.size.heigth/$("#desktop").height()) * 100;
    $(this).css("flex-basis",percentNum);

});

$("[data-windowId='down']").resizable({
        handles:"n"
}).on("resize",function( event, ui){ 
    // convert px size to percentage to maintain proper window size when resizing the desktop
    var percentNum =(ui.size.heigth/$("#desktop").height()) * 100;
    $(this).css("flex-basis",percentNum);
    // jquery resize changes top as well as height, must reset to default as we do not want top to change with flexbox
    $(this).css("top","auto");
});